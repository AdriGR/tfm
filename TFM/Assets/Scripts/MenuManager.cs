﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public GameObject controlsPanel;

    public GameObject creditsPanel;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }


    public void ControlsAppear() {

        controlsPanel.SetActive(true);
    }

    public void ControlsDissappear() {

        controlsPanel.SetActive(false);

    }

    public void CreditsAppear() {

        creditsPanel.SetActive(true);
    }

    public void CreditsDissappear() {

        creditsPanel.SetActive(false);

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPunchAttack : MonoBehaviour{

    // Datos del ataque del enemigo
    public DataEnemiesAttack statsEnemies;

    // Área de daño para el golpe
    public float punchArea = 5f;

    // Partículas de ataque
    public ParticleSystem attackParticles;

    // Contador interno para hacer la cuenta atrás
    private float counterAttack;

    // Referencia al animator
    private Animator animator;

    // Referencia al objetivo de ataque (player)
    private Transform target;

    // Referencia al enemy Behaviour
    private EnemyBehaviour enemyBehaviour;

    // Referencia al audiosource
    private AudioSource audioSource;


    // Start is called before the first frame update
    void Start(){
        // Recuperamos componentes
        animator = GetComponent<Animator>();
        enemyBehaviour = GetComponent<EnemyBehaviour>();
        audioSource = GetComponent<AudioSource>();

        // Buscamos el target
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update(){
        if (enemyBehaviour.state == EnemyBehaviour.States.Attack) {
            // Si la distancia del enemigo al player es menor que la necearia para atacar
            if(Vector3.Distance(transform.position, target.position) < statsEnemies.attackDistance) {
                // Si el contador para el ataque es menor o igual a 0, llamamos a la animación de ataque.
                if(counterAttack <= 0f) {
                    animator.SetTrigger("Attack");

                    // Reseteamos el tiempo de ataque
                    counterAttack = statsEnemies.timeToAttack;
                }
                // Si no, vamos restando tiempo.
                else {
                    counterAttack -= Time.deltaTime;
                }
            }
        }
        
    }


    /// <summary>
    /// Método que será ejecutado desde la animación para generar daño en área
    /// </summary>
    public void PunchAttack() {

        // Detectamos si hay collider en el área definida
        Collider[] colls = Physics.OverlapSphere(transform.position, punchArea);

        // Recorremos las colisiones detectadas
        foreach(Collider col in colls) {

            // Intentamos recuperar el componente PlayerHealth, PlayerController y el escudo
            PlayerHealth playerHealth = col.GetComponent<PlayerHealth>();
            PlayerController playerController = col.GetComponent<PlayerController>();
            Shield shield = col.GetComponentInChildren<Shield>();

            // Si el objetivo tiene el componente PlayerHealth, le hacemos el daño
            if(playerHealth != null && !playerController.shieldActive) {
                playerHealth.TakeDamage(statsEnemies.damage);
            } else if(playerHealth != null && playerController.shieldActive) {
                // Si no, restamos armadura del escudo
                shield.TakeShieldDamage(statsEnemies.damage);
            }
            
        }

        // Instanciamos las partículas del ataque
        Instantiate(attackParticles, transform.position, Quaternion.identity);

        // Reproducimos el sonido del golpe en el suelo
        audioSource.PlayOneShot(statsEnemies.attackSound);

        
        
        
       
    }
}

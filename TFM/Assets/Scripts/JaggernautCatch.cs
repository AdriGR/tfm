﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 17/10/2019
* Ultima fecha modificacion: 17/10/2019
* Descripcion: Script para controlar si hemos atrapado al player y realizar las animaciones correspondientes

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JaggernautCatch : MonoBehaviour{

    // Referencia donde colocaremos al player una vez cogido
    public Transform playerCatchPosition;

    // Referencia al componente JaggernautBehaviour
    private JaggernautBehaviour jaggernautBehaviour;

    // Referencia al animator del padre
    private Animator animator;

    // Referencia al rigidbody del jugador para moverlo kinemático
    private GameObject player;

    // Referencia al rigidbody del player
    private Rigidbody playerRB;

    // Referencia al capsule collider del player
    private CapsuleCollider playerCollider;

    // Start is called before the first frame update
    void Start(){
        jaggernautBehaviour = GetComponentInParent<JaggernautBehaviour>();
        animator = GetComponentInParent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerRB = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update(){
        
    }


    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")) {
            // Activamos el objeto
            playerCatchPosition.gameObject.SetActive(true);

            // Si el otro collider tiene la etiqueta de player, lo hacemos hijo y ejecutamos la animación de coger
            other.transform.SetParent(playerCatchPosition);

            // Activamos la booleana para indicar que lo hemos atrapado
            jaggernautBehaviour.playerCatch = true;

            // Ejecutamos la animación de coger
            animator.SetBool("Catch", true);

            if(jaggernautBehaviour.playerCatch){
                // Ponemos el rigidbody del player kinemático para moverlo libremente
                playerRB.isKinematic = true;
            }
            // Deshabilitamos el capsule collider
            player.GetComponent<CapsuleCollider>().enabled = false;

            // Posicionamos al jugador en el sitio de cogida
            player.transform.position = playerCatchPosition.position;

        }
    }

    

    
}

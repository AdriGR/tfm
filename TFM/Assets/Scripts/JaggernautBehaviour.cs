﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 17/10/2019
* Ultima fecha modificacion: 17/10/2019
* Descripcion: Script para controlar las acciones del jaggernaut

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class JaggernautBehaviour : MonoBehaviour{

    // Referencia donde colocaremos al player una vez cogido
    public Transform playerCatchPosition;

    // Objetivo para arrojar al jugador una vez atrapado
    public Transform secondTarget;

    public DataEnemiesAttack jaggerAttack;

    // Booleana para indicar si hemos atrapado al jugador
    public bool playerCatch;

    // Booleana para indicar si estamos paralizados
    public bool isParalized;

    // Distancia mínima para poder ejecutar el lanzamiento
    public float minDistanceToThrow = 0.1f;

    // Contador para el tiempo paralizado
    private float tempParalized = 0f;

    // Referencia al nav mesh agent
    private NavMeshAgent nav;

    // Referencia al rigidbody
    private Rigidbody rB;

    // Objetivo a atrapar
    private GameObject target;

    // Referencia al animator
    private Animator animator;

    // Obtenemos el rigidbody del player
    private Rigidbody playerRB;

    // Referencia al collider que detecta al player para agarrarlo
    private BoxCollider boxColliderTrigger;

    // Referencia al audioSource
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componentes
        nav = GetComponent<NavMeshAgent>();
        rB = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        boxColliderTrigger = GetComponentInChildren<BoxCollider>();
        audioSource = GetComponent<AudioSource>();

        // Buscamos al jugador y obtenemos su rigidbody
        target = GameObject.FindGameObjectWithTag("Player");
        playerRB = target.GetComponent<Rigidbody>();

        // Ejecutamos la animación de andar al empezar
        animator.SetTrigger("Walk");

        

    }

    // Update is called once per frame
    void Update() {
        // Si estamos paralizados, llamamos al método
        /*
        if(isParalized) {
            EnemyParalized();   
        }*/


        // Si no tenemos cogido al jugador
        if(!playerCatch) {
            // Indicamos que lo siga
            nav.SetDestination(target.transform.position);
        } else {
            // Si lo tenemos cogido, le indicamos que se dirija hacia el punto de lanzamiento
            nav.SetDestination(secondTarget.position);

            if(nav.remainingDistance < minDistanceToThrow) {
                // Ejecutamos animación de lanzar
                animator.SetTrigger("Throw");
            }
        }

        
        /*
        // Comprobamos si la posición es igual a la del objetivo de lanzamiento
        if(nav.remainingDistance < minDistanceToThrow) {
            // Ejecutamos animación de lanzar
            animator.SetTrigger("Throw");


            // Si no hemos atrapado al jugador
            if(!isCatch) {
                // Indicamos que lo siga
                nav.SetDestination(target.transform.position);

                // Miramos hacia el objetivo
                //transform.LookAt(target.transform);

                // Ejecutamos animación de andar
                animator.SetTrigger("Walk");

                // Tenemos el collider de detección activado
                boxColliderTrigger.enabled = true;
            } else {

                // Desactivamos el collider de detección
                boxColliderTrigger.enabled = false;

                // Indicamos que no estamos parados
                nav.isStopped = false;

                // Si no, le indicamos que se dirija hacia el punto de lanzamiento
                nav.SetDestination(secondTarget.position);

                // Miramos hacia el objetivo
                //transform.rotation = Quaternion.LookRotation(secondTarget.transform.forward);

                // Ejecutamos animación de andar con el player atrapado
                animator.SetTrigger("WalkCatch");

                // Comprobamos si la posición es igual a la del objetivo de lanzamiento
                if(nav.remainingDistance < minDistanceToThrow) {
                    // Ejecutamos animación de lanzar
                    animator.SetTrigger("Throw");
                }
            }

        }*/
    }

    /// <summary>
    /// Método para lanzar al player que ejecutaremos desde el animator
    /// </summary>
    public void ThrowPlayer() {
        // Indicamos que no es kinemático y que use la gravedad
        playerRB.isKinematic = false;
        playerRB.useGravity = true;

        // Desemparentamos al jugador
        target.transform.SetParent(null);

        // Obtenemos el rigidbody del player y le damos una fuerza en modo impulso
        playerRB.AddForce(secondTarget.forward * jaggerAttack.damage, ForceMode.Impulse);

        // Reproducimos sonido de lanzamiento
        audioSource.PlayOneShot(jaggerAttack.attackSound);

        // Indicamos que ya no está cogido
        playerCatch = false;
    }

   

    /// <summary>
    /// Método para indicar que estamos paralizados
    /// </summary>
    public void EnemyParalized() {

        // Indicamos que estamos paralizados
        isParalized = true;

        // Paramos el movimiento del nav mesh
        nav.isStopped = true;

        // Desactivamos el collider trigger
        boxColliderTrigger.enabled = false;

        

        // Si tenemos cogido al jugador, lo desemparentamos.
        if(playerCatch) {
            target.transform.parent = null;

            // Indicamos que el rigidbody no sea kinemático y que use gravedad
            playerRB.isKinematic = false;

            // Le decimos que no use gravedad
            playerRB.useGravity = true;

            // Le indicamos que puede moverse
            target.GetComponent<PlayerController>().blockMove = false;

            // Habilitamos el capsule collider
            target.GetComponent<CapsuleCollider>().enabled = true;
        }

        // Indicamos que ya no está cogido
        playerCatch = false;

        // Ejecutamos animación de paralizado
        animator.SetTrigger("Paralized");

        

        
    }

    /// <summary>
    /// Método que llamaremos desde el animator para volver a por el jugador
    /// </summary>
    public void FinishParalized() {
        // Indicamos que no estamos paralizados
        isParalized = false;

        // Reanudamos el nav mesh
        nav.isStopped = false;

        // Activamos el collider trigger
        boxColliderTrigger.enabled = true;

        // Reproducimos animación de andar
        animator.SetTrigger("Walk");

        // Indicamos que la booleana de coger está a falso
        animator.SetBool("Catch", false);
    }


    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")) {
            // Paramos el movimiento del nav mesh
            nav.isStopped = true;

            // Activamos la booleana para indicar que lo hemos atrapado
            playerCatch = true;

            // Ejecutamos la animación de coger
            animator.SetBool("Catch", true);

            

            

            

        }
    }

    /// <summary>
    /// Método que llamaremos en el animator para comprobar si se ha escapado el player
    /// </summary>
    public void IsPlayerScaped() {
        // Si no hemos cogido al player, indicamos que catch es falso para que vuelva a la animación de correr
        if(!playerCatch) {
            animator.SetBool("Catch", false);

            // Decimos que no estamos parados
            nav.isStopped = false;
        } else {

            // Si el otro collider tiene la etiqueta de player, lo hacemos hijo
            target.transform.SetParent(playerCatchPosition);

            // Posicionamos al jugador en el sitio de cogida
            target.transform.position = playerCatchPosition.position;

            nav.isStopped = false;

            // Si no, ponemos el rigidbody del player en kinemático para moverlo libremente
            playerRB.isKinematic = true;

            // Le decimos que no use gravedad
            playerRB.useGravity = false;

            

            // Le indicamos que no puede moverse
            target.GetComponent<PlayerController>().blockMove = true;

            // Deshabilitamos el capsule collider
            target.GetComponent<CapsuleCollider>().enabled = false;

        }
    }






}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleCube_Script : MonoBehaviour {

    public GameObject wall;

    public GameObject cubePrefab;

    public GameObject cubePosition;

    private Animator animator;

    public GameObject cam;
    public GameObject wallCam;


    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start() {

        animator = wall.GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnValidate() {

    }

    private void OnTriggerEnter(Collider other) {

        //Si hacemos que el cubo llegue a la plataforma correspondiente.
        if (other.CompareTag("PushableCube")) {

            //Activamos la animación que abre el muro.

            animator.SetTrigger("Open");

            cam.SetActive(false);
            wallCam.SetActive(true);

            audioSource.Play();

            StartCoroutine(NormalCam());
        }


    }


    private void OnCollisionEnter(Collision col) {

        // Si el cubo cae.
        if (col.gameObject.name == "DeadPlane") {
            //Lo destruimos.
            Destroy(this.gameObject, 1.5f);
            //Y creamos otro al mismo tiempo
            StartCoroutine(InstantiateCube());




        }
    }
    /// <summary>
    /// Instancia un nuevo cubo 1.5 segundos después de la caída del actual.
    /// </summary>
    /// <returns></returns>
    IEnumerator InstantiateCube() {

        yield return new WaitForSeconds(1.5f);

        Instantiate(cubePrefab, cubePosition.transform.position, cubePosition.transform.rotation);
    }


    /// <summary>
    /// Vuelve a activar la cam normal.
    /// </summary>
    /// <returns></returns>
    IEnumerator NormalCam() {

        yield return new WaitForSeconds(3f);


        cam.SetActive(true);
        wallCam.SetActive(false);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_track : MonoBehaviour {

    // Objeto al que apuntará la cámara.
    public GameObject target;

    // Distancia en el eje Z.¡ a la que se posicionará la cámara respecto al objetivo.
    public float zMargin = 10f;
    public float ymargin = 10f;
    public float xMargin = 8f;

    // Variable para suavizado del movimiento de la cámara.
    public float smoothTime = 0.2f;

    // Vector que utilizaremos para guardar la posición de la cámara en las reacciones
    public Vector3 startReactionPosition;

    // Layer contra el que colisionará el Raycast de detección de obstáculos entre el target y la cámara.
    public LayerMask wallLayer;
    // Velocidad de movimiento de la cámara.
    private Vector3 velocity = Vector3.zero;

    


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        // Guardamos la posición
        startReactionPosition = transform.position;
    }


    // calculamos la posición en la que deberá estar posicionada la cámara.
    private void FixedUpdate() {
        Vector3 tempPosition = new Vector3(target.transform.position.x - xMargin, target.transform.position.y + ymargin, target.transform.position.z - zMargin);
        // Revisamos que no haya obstáculos y si es así, que los evite.
        //CheckOcclusion(ref tempPosition);

        // Llevamos la cámara hacia la posición calculada.
        transform.position = tempPosition;

        

       

    }

    /// <summary>
    /// Verificamos que no existan obstáculos entre target y cámara. Y de existir, calculamos la posición de la cámara para evitarlo.
    /// </summary>
    /// <param name="pos"></param>
    void CheckOcclusion(ref Vector3 pos) {

        Debug.DrawLine(target.transform.position, pos, Color.red);
        //Variable vacía de tipo RayCast, para almacenar el resultado del impacto del linecast.
        RaycastHit wallHit = new RaycastHit();

        //realizamos un linecast entre target y cámara, para detectar si existe algún obstáculo.

    
        if (Physics.Linecast(target.transform.position, pos, out wallHit, wallLayer)) {
            Debug.DrawLine(wallHit.point, Vector3.up, Color.green);
            // Para esquivar el obstáculo, situamos la cámara en el punto de impacto con el obstáculo.
            pos = new Vector3(wallHit.point.x, pos.y, wallHit.point.z);
        }
    }
}

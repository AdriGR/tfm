﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 12/05/2019
* Última fecha modificación: 31/08/2019
* Descripción: Contiene los menus del juego y la administración de energía del jugador

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour{

    // Referencia al panel de pausa
    public GameObject pausePanel;

    // Referencia al panel de muerte
    public GameObject deadPanel;

    // Referencia al panel de opciones ingame
    public GameObject optionsPanel;

    // Booleana para saber si estamos pausados
    public bool pause;

    [Header("Energy Manager")]

    //Referencia al slider de energia
    public Slider energySlider;

    // Referencia al máximo de energía que puede tener
    public int maxEnergy;

    // Cantidad actual de energía
    public float actualEnergy;

    // Cantidad que puede recuperar la energía por segundos
    public int recoverEnergy;

    // Para saber si tenemos que restar energía con el tiempo
    public bool continuedEnergy;

    [Header("Health Manager")]
    // Referencia al slider de vida para recuperarlo luego en el playerHealth
    public Slider healthSlider;


    public static GameManager instance;

    

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start() {
        

        

        // Si la energía guardada en el datamánager es menor o igual a la máxima, cargamos el valor guardado, si no lo igualamos a la máxima
        actualEnergy = DataManager.instance.data.playerEnergy <= maxEnergy ? DataManager.instance.data.playerEnergy : maxEnergy;

        // Actualizamos el slider
        energySlider.value = actualEnergy;

        

        
    }



    // Update is called once per frame
    void Update(){
       if(Input.GetButtonDown("Cancel")) {
            Pause();
        }

       
    }

    public void Pause() {

        // Activamos/desactivamos el panel de pausa
        pausePanel.SetActive(!pausePanel.activeSelf);

        // indicamos que estamos en pausa
        pause = true;

        // Detenemos el tiempo
        Time.timeScale = 0f;

        // Si pulsamos escape y el menu de opciones ingame está activo.
        if (Input.GetButtonDown("Cancel") && optionsPanel.activeInHierarchy) {

            // desactivamos el menú de opciones ingame
            optionsPanel.SetActive(!optionsPanel.activeSelf);

            // Volvemos a activar (en este caso porque estaría desactivado) el menú de pausa
            pausePanel.SetActive(true);
        }

        if(Input.GetButtonDown("Cancel") && !pausePanel.activeInHierarchy) {

            // Indicamos que ya no estamos en pausa
            pause = false;

            // Volvemos a continuar el tiempo
            Time.timeScale = 1;

            
        }

        if (!pausePanel.activeInHierarchy) {
            Time.timeScale = 1f;

            pause = false;
        }
        
        
    }

    /// <summary>
    /// Método para gestionar la energía
    /// </summary>
    /// <param name="amount"></param>
    public void UseEnergy(int amount) {

        if (continuedEnergy) {
            // Restamos la cantidad indicada a la energía cada segundo
            actualEnergy -= amount * Time.deltaTime;
        } else {
            actualEnergy -= amount;
        }

        // Actualizamos el slider
        energySlider.value = actualEnergy;

        // Si la energía es menor o igual a 0, impedimos que siga bajando
        if (actualEnergy <= 0) {
            actualEnergy = 0;
        }
    }

    /// <summary>
    /// Método para recuperar energía
    /// </summary>
    public void RecoveryEnergy() {

        // Recuperamos energía a cada segundo
        actualEnergy += recoverEnergy * Time.deltaTime;

        // Actualizamos el slider
        energySlider.value = actualEnergy;

        // Si la energía es mayor o igual a la máxima, la igualamos a esta impidiendo que llegue por encima
        if (actualEnergy >= maxEnergy) {
            actualEnergy = maxEnergy;
        }
    }

    
}

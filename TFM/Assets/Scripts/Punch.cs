﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punch : MonoBehaviour{

    // Daño que hacen los golpes
    public int damage;


    /*
    /// <summary>
    /// Método para detectar colisiones
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision) {
        // Recuperamos el componente enemy health del enemigo
        EnemyHealth enemy = collision.collider.GetComponent<EnemyHealth>();

        // Si es distinto de nulo, le hacemos daño
        if(enemy != null) {
            enemy.TakeDamage(damage, transform.position);

        }

    }
    */


    private void OnTriggerEnter(Collider other) {
        // Recuperamos el componente enemy health y enemy Behaviour del enemigo
        EnemyHealth enemy = other.gameObject.GetComponent<EnemyHealth>();
        EnemyBehaviour enemyBehaviour = other.GetComponent<EnemyBehaviour>();
        
        // Si el enemy health es distinto de nulo, le hacemos daño
        if (enemy != null) {
            enemy.TakeDamage(damage, transform.position);
        }

        // Si enemy behaviour es distinto de nulo, le hacemos daño y le indicamos que nos ataque
        if(enemyBehaviour != null) { 
            enemyBehaviour.TakingDamage(transform);
            
        } 

        
    }
    
}

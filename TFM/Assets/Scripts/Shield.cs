﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 15/06/2019
* Última fecha modificación: 28/08/2019
* Descripción: Administra las funcionalidades del escudo.

*************************************************************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder usar elementos del HUD
using UnityEngine.UI;

public class Shield : MonoBehaviour{



    // Referencia al escudo bueno
    public GameObject shieldFull;

    // Referencia al escudo roto
    public ParticleSystem shieldBreakParticles;

    // Material al que cambiaremos su color según su estado de armadura
    private Renderer shieldMaterial;

    // Referencia a la resistencia total del escudo
    public int totalArmor;

    // Referencia a la resistencia actual del escudo
    public int actualArmor;

    // Cantidad que se recuperará el escudo
    public int recoverShield;

    // Para saber que el escudo se ha roto a causa de los impactos
    public bool isBroken;

    // Valor por el cual cambiará el lerpeo del color
    public float lerpValue;

    // Referencia al player Controller
    private PlayerController playerController;


    // Start is called before the first frame update
    void Start(){
        // Recuperamos componente player controller
        playerController = GetComponentInParent<PlayerController>();
        shieldMaterial = transform.GetChild(1).GetComponentInChildren<Renderer>();

        // Igualamos la resistencia actual a la total
        actualArmor = totalArmor;

        // Empezamos con color verde indicando que está al maximo de armadura
        shieldMaterial.material.SetColor("_Color", Color.green);
        
    }

    // Update is called once per frame
    void Update(){

        

        // Si está roto, llamamos al método que lo recupera
        if (isBroken) {
            RecoveryShield();
        }
        
    }

    /// <summary>
    /// Método para romper el escudo
    /// </summary>
    public void BrokenShield() {

        // Indicamos que el escudo se ha roto
        isBroken = true;

        // Si el valor de la armadura/escudo es igual o menor que 0, desactivamos el escudo
        if(actualArmor <= 0) {
            shieldFull.SetActive(false);
            // Reproducimos sistema de partículas
            shieldBreakParticles.Play();

            // Indicamos tambíen que el escudo ya no se está utilizando
            playerController.shieldActive = false;
        } 

    }

    public void RecoveryShield() {
        // Vamos recuperando la armadura del escudo con el paso del tiempo
            actualArmor += recoverShield * Mathf.FloorToInt(Time.deltaTime);

        // Si la armadura está recuperada al máximo, indicamos que ya no está roto
        if (actualArmor >= totalArmor) {
            isBroken = false;
        }
    }

    public void TakeShieldDamage(int amount) {
        // Aplicamos el daño recibido
        actualArmor -= amount;

        // Cambiamos el color del escudo según su cantidad de armadura restante
        if(actualArmor <= 20f) {
            shieldMaterial.material.SetColor("_Color", Color.red);
            // En el caso de que esté en un valor de 60 o menos, cambiará el color de amarillo a rojo
        } else if(actualArmor <= 60f) {
            shieldMaterial.material.SetColor("_Color", Color.Lerp(Color.yellow, Color.red, lerpValue));
            // En otro caso, el color del escudo será verde y lo iremos cambiando a amarillo
        } else {
            shieldMaterial.material.SetColor("_Color", Color.Lerp(Color.green, Color.yellow, lerpValue));
        }

        // Si la armadura actual es menor o iguala 0
        if (actualArmor <= 0) {
            // Llamamos al método que rompe el escudo
            BrokenShield();
        }
    }
}

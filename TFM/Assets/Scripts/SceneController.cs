﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder usar cambios de escena
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    // canvas para hacer el oscurecido de la pantalla
    public CanvasGroup faderCanvasGroup;
    // duración del fundido
    public float fadeDuration = 1f;

    // Para saber si se realizará la oclusión
    public bool isOcclusion;

    // Nombre de la escena donde empezará y su posición
    public string startingScene = "Game";
    public string initialStartingPositionName = "InitialPosition";


    // para indicar si se está realizando un fade
    private bool isFading;

    // Referencia a la animación de oclusión
    private Animator occluderAnimation;

    public static SceneController instance;


    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Start is called before the first frame update
    private IEnumerator Start() {

        // Recuperamos el componente animator del canvas
        occluderAnimation = GameObject.Find("PersistentCanvas").GetComponent<Animator>();

        // Comprobamos si el DataMánager tiene cargada información de la escena actual, esto significaría que viene de una escena guardada
        startingScene = DataManager.instance.data.actualScene != "" ? DataManager.instance.data.actualScene : startingScene;

        // Compruebo si el DataMánager tiene una información cargada de la posición de entrada, esto significaría que viene de una partida guardada
        initialStartingPositionName = DataManager.instance.data.entrancePosition != "" ? DataManager.instance.data.entrancePosition : initialStartingPositionName;

        // Cargamos la escena y la ponemos activa, esperando a que termine
        yield return StartCoroutine(LoadSceneAndSetActive(startingScene));

        // Una vez cargada la escena, hacemos Fade In
        StartCoroutine(Fade(0f));
    }

    // Update is called once per frame
    void Update() {

    }


    /// <summary>
    /// Realiza el fundido a visible o invisible
    /// </summary>
    /// <param name="finalAlpha"></param>
    /// <returns></returns>
    private IEnumerator Fade(float finalAlpha) {
        // indicamos que se está realizando un fundido
        isFading = true;
        // bloqueamos todo raycast para evitar que el jugador interactue durante el fade
        faderCanvasGroup.blocksRaycasts = true;
        // calculo la velocidad
        float fadeSpeed = 1f / fadeDuration;
        // mientras el alpha del canvas no sea igual al alpha destino
        while (!Mathf.Approximately(faderCanvasGroup.alpha, finalAlpha)) {
            // movemos poco a poco el alpha actual hacia el destino
            faderCanvasGroup.alpha = Mathf.MoveTowards(faderCanvasGroup.alpha,
                                                       finalAlpha,
                                                       fadeSpeed * Time.deltaTime);

            yield return null;
        }
        // por si el resultado final no es el valor exacto, realizamos esta asignación
        faderCanvasGroup.alpha = finalAlpha;
        // indico que el fade ha terminado
        isFading = false;
        // eliminamos el bloqueo del raycast para que el jugador pueda interactuar
        faderCanvasGroup.blocksRaycasts = false;
    }


    /// <summary>
    /// Carga una escena de forma asíncrona
    /// </summary>
    /// <param name="sceneName"></param>
    /// <returns></returns>
    private IEnumerator LoadSceneAndSetActive(string sceneName) {
        // cargamos la escena de forma aditiva, sin destruir la escena principal
        yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        // recuperamos la última escena cargada
        Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
        // marcamos esta escena como activa
        SceneManager.SetActiveScene(newlyLoadedScene);
    }


    /// <summary>
    /// Hace un fade y cambia la escena
    /// </summary>
    /// <param name="sceneName"></param>
    /// <returns></returns>
    private IEnumerator FadeAndSwitchScenes(string sceneName) {
        // Fade out, y hacemos yield return para esperar a que termine
        yield return StartCoroutine(Fade(1f));
        // una vez terminado el fade, descargamos la escena activa
        yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        // una vez terminada la descarga, cargamos y activamos la escena indicada
        yield return StartCoroutine(LoadSceneAndSetActive(sceneName));

        // tras terminar la carga, realizamos un fade in
        yield return StartCoroutine(Fade(0f));
    }


    /// <summary>
    /// Llamada pública para el cambio de escena
    /// </summary>
    /// <param name="sceneName"></param>
    /// <param name="entrancePosition"></param>
    public void FadeAndLoadScene(string sceneName, string entrancePosition) {
        // actualizamos el valor de la escena actual
        DataManager.instance.data.actualScene = sceneName;
        // actualizamos el punto de posicionamiento del player
        DataManager.instance.data.entrancePosition = entrancePosition;
        // guardamos al realizar un cambio de escena
        DataManager.instance.Save();
        // si no se está haciendo fade
        if (!isFading) {
            // cambiamos de escena
            StartCoroutine(FadeAndSwitchScenes(sceneName));
        }
    }

    /// <summary>
    /// Método para cambiar de escenas
    /// </summary>
    /// <param name="scene"></param>
    public void ChangeScenes(string scene) {
        // Cambiamos de escena a la indicada en el parámetro
        SceneManager.LoadScene(scene);

        //FadeAndLoadScene(scene, "");
    }

    /// <summary>
    /// Ejecuta la animación de entrada de oclusión para la cámara
    /// </summary>
    public void AnimationOcclusion() {
        // Si la booleana está activa, realizamos la animación de entrada
        if(isOcclusion) {
            occluderAnimation.SetTrigger("OccluderIn");
        } else {
            // Si no, realizamos la animación de salida
            occluderAnimation.SetTrigger("OccluderOut");


        }
        
    }


    /// <summary>
    /// Método para salir del juego
    /// </summary>
    public void ExitGame() {
        Application.Quit();
    }

    /// <summary>
    /// Método para cargar la escena activa si hemos muerto
    /// </summary>
    public void Retry() {
        // Llamamos al método que hace fade y cambia de escena, hacia la escena actualmente activa
        FadeAndSwitchScenes(SceneManager.GetActiveScene().name);
    }
}


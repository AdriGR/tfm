﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 01/09/2019
* Ultima fecha modificacion: 18/09/2019
* Descripcion: Controla la salud del enemigo,cuando está muerto y las acciones a realizar cuando recibe daño

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Referencia para poder utilizar el navmesh agent.
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {

    // Vida máxima del enemigo
    public float maxHealth;

    // Vida actual del enemigo
    public float actualHealth;

    // Referencia al slider que indica la vida
    public Image healthImage;

    // Prefab del charco de sangre
    public SpriteRenderer bloodPrefab;

    // Para saber si el enemigo ha muerto
    public bool isDead;

    // Valor del fillamount de la imagen de sangre para rellenarla
    //public float bloodImageValor;



    [Header("Partículas")]

    // Referencia donde se instanciaran los efectos
    public Transform effects;
    
    // Referencia al sistema de partículas cuando recibe daño
    public ParticleSystem damageParticles;

    // Referencia a las partículas de muerte
    public ParticleSystem deathParticles;

    // Referencia a las partículas de explosión
    public ParticleSystem explosionDeathParticles;

    

    [Header("Sonidos")]
    // Sonido de daño
    public AudioClip hitSound;

    // Sonido de muerte
    public AudioClip deathSound;

    // Sonido de la explosión de destrucción
    public AudioClip explosionSound;

    // Referencia al componente nav mesh
    private NavMeshAgent nav;

    // Referencia al componente animator
    private Animator animator;

    // Referencia al audioSource
    private AudioSource audioSource;

    // Referencia al enemyBehaviour
    private EnemyBehaviour enemyBehaviour;

    // Referencia al rigidbody
    private Rigidbody rB;

    // Referencia al capsule collider
    private CapsuleCollider capsuleCollider;

    

    


    //Transform t;
    //public float fixedRotation = 5;

    void Start() {
        // Recuperamos componentes
        nav = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        enemyBehaviour = GetComponent<EnemyBehaviour>();
        rB = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        
        //t = transform;
        // Inicializamos la vida actual a la máxima
        actualHealth = maxHealth;

        
    }

    void Update() {
        //Con esto bloquearemos el movimiento del enemigo en x.
       // t.eulerAngles = new Vector3(fixedRotation, t.eulerAngles.y, t.eulerAngles.z);

        // Hacemos que la barra de vida mire a la cámara
        healthImage.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);

    }


    /// <summary>
    /// Método para las acciones a realizar cuando el enemigo sea dañado
    /// </summary>
    /// <param name="amount"></param>
    public void TakeDamage(int amount, Vector3 positionImpact) {
        // Si el enemigo está muerto, no hacemos nada
        if(isDead) {
            return;
        }

        // Posicionamos el sistema de partículas en el lugar donde hemos recibido el impacto
        damageParticles.transform.position = positionImpact;

        // Reprocimos las partículas
        damageParticles.Play();

        // Reproducimos sonido de daño
        audioSource.PlayOneShot(hitSound);

        // Reducimos la cantidad de vida
        actualHealth -= amount;

        // Actualizamos la imagen
        healthImage.fillAmount = actualHealth / maxHealth;

        // Si la vida actual es menor o igual a 0 y no está muerto, llamamos al método de muerte
        if (actualHealth <= 0 && !isDead) {
            Death();
        }
    }

    /// <summary>
    /// Método para controlar las acciones al morir el enemigo
    /// </summary>
    public void Death() {
        // Indicamos que ha muerto
        isDead = true;

        // Detenemos el movimiento del nav mesh agent y lo desactivamos, al igual que el componente que hace que se encare al player.
        nav.isStopped = true;
        nav.enabled = false;

        // Desactivamos el movimiento del enemigo para impedir movimientos despues de muerto
        enemyBehaviour.enabled = false;

        // Desactivamos el capsule collider
        capsuleCollider.enabled = false;

        // Ponemos el rigidbody en kinemático para que cuando haga la animación de muerte se quede en el sitio y no de vueltas
        rB.isKinematic = true;

        // Reproducimos animación de muerte
        animator.SetTrigger("Dead");

        
    }

    /// <summary>
    /// Método que sera llamado desde el animator al caer el enemigo al suelo para producir los efectos de muerte
    /// </summary>
    public void EffectsDeath() {

        // Reproducimos sonido de muerte
        audioSource.PlayOneShot(deathSound);

        // Instanciamos la mancha de sangre
        Instantiate(bloodPrefab, effects.transform.position, Quaternion.LookRotation(bloodPrefab.transform.forward));

        // Posicionamos el sistema de partículas de muerte en el centro del enemigo
        deathParticles.transform.position = effects.transform.position;

        // Reproducimos partículas de muerte
        deathParticles.Play();

        // LLamamos al método que controla la destrucción a los 3 segundos
        Invoke("ExplosionRobot", 3f);
        
    }


    /// <summary>
    /// Método para simular la explosión del robot y quitarlo de la escena
    /// </summary>
    public void ExplosionRobot() {

        // Reproducimos sonido de explosión
        audioSource.PlayOneShot(explosionSound);

        // Reproducimos el sistema de particulas de explosion
        explosionDeathParticles.Play();

        // Destruimos el objeto a los 2 segundos 
        Destroy(this.gameObject, 1.5f);
    }
}

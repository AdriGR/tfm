﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 01/09/2019
* Ultima fecha modificacion: 07/09/2019
* Descripcion: Controla la salud del jugador,cuando está muerto y las acciones a realizar cuando recibe daño

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    // Vida máxima del jugador.
    public int maxHealth = 50;

    // Vida actual del jugador.
    public float currentHealth;

    // Para controlar cuando el jugador ha muerto.
    public bool isDead;
    // Para controlar cuando el jguador ha sufrido daño.
    private bool damaged;

    [Header("Sonidos")]
    // Sonido de muerte
    public AudioClip deathSound;
    public AudioClip damageSound;


    // Referencia a los componentes de control, para desactivarlos llegado el momento.
    private PlayerController playerController;
    
    // Referencia al animator
    private Animator animator;

    // Referencia al AudioSource
    private AudioSource audioSource;




    // Use this for initialization
    void Start() {
        // Recuperamos componentes
        playerController = GetComponent<PlayerController>();
        animator = GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();


        currentHealth = DataManager.instance.data.playerHealth <= 0 ? maxHealth : DataManager.instance.data.playerHealth;
        /*
        // Si la vida actual guardada en el gameManager es menor o igual que 0
        if (GameManager.instance.healthSlider.value <= 0) {
            // Igualamos la vida actual a la máxima
            currentHealth = maxHealth;
        } else {
            // Si no, igualamos la vida actual a la que haya en el gameManager
            currentHealth = GameManager.instance.healthSlider.value;
        }*/
        

        

        // Igualamos el valor del slider a la vida actual
        GameManager.instance.healthSlider.value = currentHealth;
        

    }

    // Update is called once per frame
    void Update() {

    }

    /// <summary>
    /// Método que aplica el daño recibido como parámetro.
    /// </summary>
    /// <param name="amount"></param>
    public void TakeDamage(float amount) {
        // Indicamos que el jugador acaba de recibir daño.
        //damaged = true;

        // Aplicamos el daño recibido.
        currentHealth -= amount;

        // Actualizamos el estado de la barra de vida.
        GameManager.instance.healthSlider.value = currentHealth;
        
        // Reproducimos sonido de daño
        audioSource.PlayOneShot(damageSound);

        // Ejecutamos animacion de recibir daño
        animator.SetTrigger("Damage");

        // Si la vida actual es menos o igual a 0 y no está muerto
        if (currentHealth <= 0 && !isDead) {

            // Realizamos las acciones de muerte.
            Death();

        }
         
    }

    /// <summary>
    /// Método para recibir daño por el efecto del puzzle sin realizar animación de daño ni sonido
    /// </summary>
    /// <param name="amount"></param>
    public void TakeDamagePuzzle(float amount) {
        // Aplicamos el daño recibido
        currentHealth -= amount;

        // Actualizamos el estado de la barra de vida
        GameManager.instance.healthSlider.value = currentHealth;

        // Si la vida llega a 0 o menos y no está muerto, realizamos las acciones de muerte
        if(currentHealth <= 0 && !isDead) {
            Death();
        }
    }

    /// <summary>
    ///  Método que se encargará de realizar las acciones necesarias para la muerte del jugador.
    /// </summary>
    public void Death() {

        // Indicamos que el jugador ha muerto.
        isDead = true;

        // Desactivamos el control del jugador para que no pueda moverse
        playerController.enabled = false;

        // Cambiamos la etiqueta del jugador a no etiquetado para que el enemigo pueda volver al estado de patrulla
        playerController.tag = "Untagged";

        // Reproducimos animación de muerte
        animator.SetTrigger("Death");

        // Mostramos panel de muerte
        GameManager.instance.deadPanel.SetActive(true);

    }

    
}



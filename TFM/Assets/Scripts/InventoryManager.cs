﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 04/09/2019    
* Ultima fecha modificacion: 04/09/2019
* Descripcion: Administra las funciones del inventario

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// para usar elementos UI
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

    // Referencia al canvas del inventario
    public GameObject inventoryCanvas;

    public static InventoryManager instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    private void Start() {
        
        
    }

    /// <summary>
    /// Método para añadir items al inventario
    /// </summary>
    /// <param name="itemName"></param>
    public bool AddItemInventory(string itemName) {
        // Variable donde se almacenará la información del item a añadir
        Item newItem = new Item();

        // Recorremos todos los items y miramos si hay alguno que tenga el mismo nombre
        foreach (Item item in DataManager.instance.data.allItem) {
            if (item.name == itemName) {
                newItem.name = item.name;
                newItem.description = item.description;
                newItem.imageName = item.imageName;
                newItem.picked = true;

                // Como ya hemos localizado el item, salimos del bucle
                break;
            }
        }

        

        switch(itemName) {
            case "Sword":
                DataManager.instance.data.inventory[0] = newItem;
                UpdateInventory();
                return true;


            case "Gun":
                DataManager.instance.data.inventory[1] = newItem;
                UpdateInventory();
                return true;


            case "Shield":
                DataManager.instance.data.inventory[2] = newItem;
                UpdateInventory();
                return true;

            case "Bomb":
                DataManager.instance.data.inventory[3] = newItem;
                UpdateInventory();
                return true;

            default:
                return false;
        }

        

        /*
        // Recorremos el inventario en busca de un hueco vacío en el que poner el item
        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++) {
            // Si encontramos un hueco con el nombre de espada
            if (DataManager.instance.data.inventory[i].nameItem == "Sword") {
                
                // Actualizamos el inventario para que muestre por pantalla el nuevo objeto
                UpdateInventory();
                // Devuelvo true al haber podido encontrar un hueco en el inventario
                return true;
            }
        }

        
        // Recorremos el inventario en busca de un hueco vacío en el que poner el item
        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++) {
            // Si encontramos un hueco con el nombre de espada
            if (DataManager.instance.data.inventory[i].nameItem == "Shield") {
                DataManager.instance.data.inventory[i] = newItem;
                // Actualizamos el inventario para que muestre por pantalla el nuevo objeto
                UpdateInventory();
                // Devuelvo true al haber podido encontrar un hueco en el inventario
                return true;
            }
        }*/


        // No había hueco en el inventario y no se ha podido recoger el objeto
        //return false;
    }

    /// <summary>
    /// Método para actualizar visualmente los objetos que serán mostrados en el canvas de inventario
    /// </summary>
    public void UpdateInventory() {
        // Recorremos cada uno de los elementos del inventario
        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++) {
            // Recuperamos el componente image del item dentro del slot correspondiente a cada elemento del array del inventario
            Image tempImage = inventoryCanvas.transform.GetChild(i).Find("Item").GetComponent<Image>();
            // Si el nombre del item es distinto de nulo, significará que ese hueco del inventario contiene un item
            if (DataManager.instance.data.inventory[i].name != "") {
                // Cargamos la imagen correspondiente en el slot
                tempImage.sprite = Resources.Load<Sprite>("Items/" + DataManager.instance.data.inventory[i].imageName);
                // Si tiene contenido, dejamos la imagen activa
                tempImage.gameObject.SetActive(true);
            } else {
                // Si no hay contenido en ese slot, ocultamos la imagen para que no se muestre nada
                tempImage.gameObject.SetActive(false);
            }
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuAppear : MonoBehaviour {

    // Referencia a los botones del menu
    public GameObject buttons;



   void Start() {

        // Hacemos que el menú aparezca al segundo de empezar la escena
        Invoke("ButtonsMenuAppear", 1f);
   }

    private void Update() {
        
    }


    /// <summary>
    /// Método para cambiar de escena a la persistent
    /// </summary>
    /// <param name="scene"></param>
    public void LoadScene(string scene) {
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    /// Método para cerrar el juego
    /// </summary>
    public void QuitGame() {
        Application.Quit();
    }

    /// <summary>
    /// Método para que aparezca el menú
    /// </summary>
    public void ButtonsMenuAppear() {
        buttons.SetActive(true);
    }

}
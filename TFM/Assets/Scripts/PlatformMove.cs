﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour
{

    public Vector2 startPosition;

    public Vector2 newPosition;

   public float speed = 3;
    private int maxDistance = 7;

    void Start() {
        startPosition = transform.position;
        newPosition = transform.position;
    }

    void Update() {
        newPosition.x = startPosition.x + (maxDistance * Mathf.Sin(Time.time * speed));
        transform.position = newPosition;
    }
}
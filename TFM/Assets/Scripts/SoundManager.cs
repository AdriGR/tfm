﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 31/08/2019
* Ultima fecha modificacion: 31/08/2019
* Descripcion: Administra los sonidos, tanto música como efectos

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder usar el audiomixer
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour{

    // Referencia al audiomixer que contiene el volumen de musica y efectos
    public AudioMixer audioMixer;

    // Referencia al audioSource
    private AudioSource audioSourceMusic;

    // Referencia al audioSource del player para los efectos de sonido
    private AudioSource audioSourceEffects;

    public static SoundManager instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }

    }

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componentes
        audioSourceMusic = GetComponent<AudioSource>();
        

    }

    // Update is called once per frame
    void Update(){
        
    }

    /// <summary>
    /// Método que modificará el volumen de la música
    /// </summary>
    /// <param name="audioSound"></param>
    public void SoundVolume(float soundVolume) {
        // Guardamos el valor del sonido del parámetro indicado
        audioMixer.SetFloat("soundVolume", soundVolume);

        // actualizamos el volúmen del audioSource
        audioSourceMusic.volume = soundVolume;

        // Almacenamos el valor en el dataManager
        DataManager.instance.data.soundVolume = soundVolume;
    }


    /// <summary>
    /// Metodo que modificará el volúmen de los efectos de sonido
    /// </summary>
    /// <param name="effectsVolume"></param>
    public void EffectsVolume(float effectsVolume) {
        // Guardamos el valor del efecto de sonido del parámetro indicado
        audioMixer.SetFloat("effectsVolume", effectsVolume);

        // Actualizamos el volúmen del audioSource
        audioSourceEffects.volume = effectsVolume;

        // Guardamos el valor en el dataManager
        DataManager.instance.data.effectsVolume = effectsVolume;
    }
}

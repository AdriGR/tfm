﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 27/08/2019
* Ultima fecha modificacion: 27/08/2019
* Descripcion: Lee el archivo xml que contiene los textos

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para leer el fichero xml
using System.Xml;

public class TranslateManager : MonoBehaviour{

    // Idioma por defecto
    public string defaultLanguage = "English";

    public string forcedLanguage = "";

    // Listado de items tipo array, pero que permite el uso de índices alfanuméricos
    public Hashtable strings;

    public static TranslateManager instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start(){
        // Recuperamos el lenguaje del sistema en un string
        string language = Application.systemLanguage.ToString();

        // Si se ha definido un idioma forzado, intentamos recuperarlo
        language = (forcedLanguage != "") ? forcedLanguage : language;

        // Recuperamos el fichero xml como texto
        TextAsset textAsset = (TextAsset)Resources.Load("Lang", typeof(TextAsset));

        // Creamos una variable de tipo xmlDocument para gestionar el xml
        XmlDocument xml = new XmlDocument();

        // Cargamos el xml desde el fichero de texto
        xml.LoadXml(textAsset.text);

        if (xml.DocumentElement[language] == null) {
            // Si no existe el idioma que el usuario tiene en su máquina, cargamos el idioma por defecto
            language = defaultLanguage;
        }

        // Llamada al método que hace la carga de literales del idioma
        SetLanguage(xml, language);
    }

    // Update is called once per frame
    void Update(){
        
    }

    /// <summary>
    /// Método que carga el idioma seleccionado del xml dentro del hashtable
    /// </summary>
    /// <param name="xml"></param>
    /// <param name="language"></param>
    public void SetLanguage(XmlDocument xml, string language) {
        // Inicializamos el hashtable
        strings = new Hashtable();

        // Recuperamos el bloque de frases del idioma seleccionado
        XmlElement element = xml.DocumentElement[language];

        // Si es distinto de null, significará que hemos recuperado textos
        if (element != null) {
            // Mediante este método recuperamos un tipo enumerador que nos permitirá recorrer los elementos del xml como si fuera un foreach
            IEnumerator elemNum = element.GetEnumerator();

            // Iremos recorriendo el xml mientras queden elementos por recorrer
            while(elemNum.MoveNext()) {
                // Obtenemos el valor de cada una de las frases
                XmlElement xmlItem = (XmlElement)elemNum.Current;

                // Recuperamos los valores del xml y los almacenamos en el hashtable
                strings.Add(xmlItem.GetAttribute("name"), xmlItem.InnerText);
            }
        } else {
            Debug.LogWarning("El lenguage especificado no existe: " + language);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public string GetString(string name) {
        // Verificamos si no existe la key
        if(!strings.ContainsKey(name)) {
            // Aviso del error
            Debug.LogWarning("La cadena no existe: " + name);

            // Salgo del método
            return "";
        }

        // Si llegamos a este punto significará que la cadena existe, así que devuelvo el valor que contiene
        return strings[name].ToString();
    }
}

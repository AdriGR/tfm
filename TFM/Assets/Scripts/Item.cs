﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item{

    // Nombre del item
    public string name;
    // Descripción para uso interno
    public string description;
    // Nombre de la imagen a recuperar de resources
    public string imageName;
    // Indica si el objeto ha sido recogido o no, esto será utilizado solo en el array de all items
    public bool picked = false;
}

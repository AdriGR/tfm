﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Referencia para poder utilizar el navmesh agent.
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyGunHealth : MonoBehaviour {

    // Vida máxima del enemigo.
    public int startingHealth = 200;

    // Vida actual del enemigo.
    public int currentHealth ;

    // Velocidad de hundimiento, para hacer que desaparezca de la escena. 
    public float sinkSpeed = 1;

    // Tiempo que tarda en destruirse el enemigo una vez empieza a hundirse.
    public float destroyDelay = 5f;
    

    // Referencia al capsule collider, necesaria para desactivarla cuando el enemigo muera.
    private CapsuleCollider capsuleCollider;

    // Referencia al Animator para lanzar animaciones.
    private Animator animator;



    // Referencia al componente EneyMovement, para desactivarlo cuando muerta.
    private EnemyBehaviour enemyMovement;

    // Referencia al componente navmesh agentm para prararlo cuadno el enemigo muera.
    private NavMeshAgent nav;

    // Variable para indicar cuando el enemigo ha muerto.
    public bool isDead;

    // Variable para indicar cuando el enemigo se está hundiendo.
    private bool isSinking;

    
    // Use this for initialization
    void Start() {
        
        animator = GetComponentInChildren<Animator>();
        
        nav = GetComponent<NavMeshAgent>();
        
        capsuleCollider = GetComponent<CapsuleCollider>();

        enemyMovement = GetComponent<EnemyBehaviour>();

        startingHealth = currentHealth;

        
    }

    // Update is called once per frame
    void Update() {

        if (isSinking) {

            // Desplazarsmos hacia abajo del enemigo considerando la velocidad a la que tiene que hacerlo.
            transform.Translate(Vector3.down * sinkSpeed * Time.deltaTime);

        }
        
    }

    /// <summary>
    /// Aplicar el daño recibido como parámetro y posicionar el sistema de partículas en la posición recibida como parámetro.
    /// </summary>
    /// <param name="amout"></param>
    /// <param name="hitPoint"></param>
    public void TakeDamage(int amount) {
        
        // Si está muerto, no hacemos nada.
        if (isDead) {
            return;
            
        }

        // Aplicamos el daño.
        currentHealth -= amount;
        

        // Si la vida del enemigo es igual o menor a cero.
        if (currentHealth <= 0 && !isDead) {
            // Ejecutamos las acciones de muerte.
            Death();


        }
        
    }
    /// <summary>
    /// Realiza las acciones necesarias para gestionar la muerte del enemigo.
    /// </summary>
    void Death() {

        // Indicamos que el enemigo ha muerto.
        isDead = true;
     

        // Eliminamos el componente capsule collider.
        Destroy(capsuleCollider);
        

        enemyMovement.enabled = false;

        // Detenemos el movimiento del navmesh agent.
        nav.isStopped = true;

        // Desactivamos el navmesh agent, para que nos permita hundir al enemigo.
        nav.enabled = false;

        // Reproducimos la animación de muerte.
        animator.SetTrigger("Dead");

        StartSinking();


    }

    /// <summary>
    /// Método que inicia el hundimiento del enemigo.
    /// </summary>
    public void StartSinking() {

        // Indicamos que le enemigo se está hundiendo.
        isSinking = true;

        // Destruimos al enemigo pasado un tiempo prudencial.
        Destroy(gameObject, 5f);



    }
}

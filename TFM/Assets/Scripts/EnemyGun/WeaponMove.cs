﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMove : MonoBehaviour {

    // Cantidad aplicada al desplazamiento del ratón
    public float amount = 0.02f;

    // Desplazamientro máximo permitido por update, para limitar el desplazamientro en giros rápidos
    public float max = 0.06f;

    // Suavizado del desplazamiento
    public float smooth = 6f;

    // Variable para guardar la posición inicial del arma
    private Vector3 initialPosition;

	// Use this for initialization
	void Start () {

        // Guardamos la posición inicial del arma
        initialPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {

        // El movimiento aplicado será  el opuesto al realizado con el ratón, modificado mediante el amount
        float movementX = -Input.GetAxis("Mouse X") * amount;
        float movementY = -Input.GetAxis("Mouse Y") * amount;

        // Hacemos clamp del movimiento máximo
        movementX = Mathf.Clamp(movementX, -max, max);
        movementY = Mathf.Clamp(movementY, -max, max);

        // Calculamos la posición final del arma
        Vector3 finalPosition = new Vector3(movementX, movementY, 0f);

        // Realizamos el movimiento desde la posición actual hasta la posición calculada
        transform.localPosition = Vector3.Lerp(transform.localPosition, initialPosition + finalPosition, Time.deltaTime * smooth);
    }
}

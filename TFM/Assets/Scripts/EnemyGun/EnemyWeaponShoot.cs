﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponShoot : MonoBehaviour {

    // Alcance del arma
    public float range = 50f;
    // Fuerza del impacto
    public float impactForce = 30f;
    // Partículas de disparo del arma
    public ParticleSystem gunsShoot;
    // Prefab de partículas del impacto
    public GameObject impactParticlePrefab;
    // Prefab del decal de impacto
    public GameObject impactDecalPrefab;
    // Capas impactables por el disparo
    public LayerMask shootableLayer;
    // Duración de los decal
    public float decalLifeTime = 20f;
    // retardo entre disparos
    // tiempo para realizar el siguiente disparo
    private float nextTimeToShoot = 0f;
    // Tamaño del cargador
    public int magazineSize = 15;
    // Balas en el cargador
    public int magazine;

    // Referencia al Animator
    private Animator animator;
    // Referencia al Audio Source
    private AudioSource audioSource;

    public bool isReloading;

    private GameObject player;
    private PlayerHealth playerHealth;
    // Objeto usado para el raycast
    public GameObject eye;
    public int attackDamage = 5;
    // Use this for initialization
    void Start() {

        // Recuperamos la referencia a los componentes
        animator = GameObject.FindGameObjectWithTag("Zaml").GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        player = GameObject.Find("Player");
        playerHealth = (PlayerHealth)player.GetComponent(typeof(PlayerHealth));
        // Iniciamos con el cargador completo
        magazine = magazineSize;
    }

    // Update is called once per frame
    void Update() {

    }

    /// <summary>
    /// Realiza los cálculos de disparo
    /// </summary>
    /// 
    [ContextMenu("CallShootTest")]
    public void CallShoot() {

        // Si no ha pasado el tiempo de espera
        if (Time.time < nextTimeToShoot || isReloading) {

            return;
        }

        nextTimeToShoot = (Time.time + (Random.Range(0.5f, 3f)));

        // Si se ha terminado el cargador
        if (magazine <= 0) {

            // Recargamos
            CallReload();

            return;
        }

        magazine--;

        gunsShoot.Play();
        audioSource.Play();
        animator.SetTrigger("Shoot");

        // Variable para almacenar el resultado del impacto
        bool impact = false;
        // Variable Raycasthit para almacenar la información del impacto
        RaycastHit hitInfo;

        impact = Physics.Raycast(eye.transform.position, eye.transform.forward, out hitInfo, shootableLayer);

        if (impact) {
            if (playerHealth.currentHealth > 0) {
                // Le hacemos el daño configurado.
                playerHealth.TakeDamage(attackDamage);

                //
                Instantiate(impactParticlePrefab, hitInfo.point, Quaternion.LookRotation(hitInfo.normal));

                GameObject decalTemp = Instantiate(impactDecalPrefab,
                                                   hitInfo.point + hitInfo.normal * 0.01f,
                                                   Quaternion.LookRotation(-hitInfo.normal));

                decalTemp.transform.parent = hitInfo.transform;

                // Rotamos aleatoriamente el decal en el eje Z
                decalTemp.transform.Rotate(new Vector3(0f, 0f, Random.Range(0f, 360f)));

                Destroy(decalTemp, decalLifeTime);

                // Si tiene rigidbody
                if (hitInfo.rigidbody != null) {

                    // Le aplicamos la fuerza en la dirección del disparo
                    hitInfo.rigidbody.AddForce(eye.transform.forward * impactForce, ForceMode.Impulse);
                }
            }

        }
    }
        public void CallReload() {


            animator.SetTrigger("Reload");
            isReloading = true;
        }

        public void EndReload() {

            magazine = magazineSize;

            isReloading = false;
        }
    
}

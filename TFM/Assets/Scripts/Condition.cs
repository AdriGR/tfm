﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Condition{

    // Nombre de la condición
    public string name;

    // Descripción de apoyo
    public string description;

    // Indicador para almacenar si se ha cumplido o no la condición
    public bool done;
}

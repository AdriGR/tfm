﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 12/05/2019
* Última fecha modificación: 31/08/2019
* Descripción: Contiene los menus del juego y la administración de energía del jugador

*************************************************************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder serializar en binario
using System.Runtime.Serialization.Formatters.Binary;
// Para poder leer/grabar en ficheros
using System.IO;

public class DataManager : MonoBehaviour{
   

    // Objeto que contendrá todas las condiciones y variables de la aplicación
    public Data data;

    // Nombre del fichero al guardar
    public string fileName = "data.dat";

    public static DataManager instance;


    private void Awake() {
        if (instance == null) {
            instance = this;

            Load();
        }

        Debug.Log(Application.persistentDataPath);
    }

    /// <summary>
    /// Método que realiza el guardado de forma persistente
    /// </summary>
    public void Save() {
        

        // Objeto utilizado para serializar/deserializar
        BinaryFormatter bf = new BinaryFormatter();
        // Creamos/sobreescribimos el fichero con los datos
        FileStream file = File.Create(Application.persistentDataPath + "/" + fileName);
        // Serializamos el contenido de nuestro objeto de datos al fichero
        bf.Serialize(file, data);
        // Al finalizar, cerramos el fichero
        file.Close();
    }

    /// <summary>
    /// Método que realiza la carga del contenido almacenado de forma persistente
    /// </summary>
    public void Load() {
        // Si no existe el fichero salgo del método
        if (!File.Exists(Application.persistentDataPath + "/" + fileName)) {
            return;
        }

        // Objeto utilizado para serializar/deserializar
        BinaryFormatter bf = new BinaryFormatter();
        // Abrimos el fichero para lectura
        FileStream file = File.Open(Application.persistentDataPath + "/" + fileName, FileMode.Open);
        // Deserializamos el fichero y lo volcamos al data
        data = (Data)bf.Deserialize(file);
        // Una vez terminado, cerramos el fichero
        file.Close();
        
    }

    /// <summary>
    /// Método que devuelve el estado en el que se encuentra la condición indicada como parámetro
    /// </summary>
    /// <param name="conditionName"></param>
    /// <returns></returns>
    public bool CheckCondition(string conditionName) {
        // Buscamos la condición en la lista de condiciones
        foreach(Condition condition in data.allCondition) {
            // Si encontramos la condición, devolvemos su estado
            if (condition.name == conditionName) {
                return condition.done;
            }
        }
        Debug.LogWarning(conditionName + " - No existe esta condición");
        return false;
    }

    /// <summary>
    /// Método que cambia el estado de una condición al estado indicado
    /// </summary>
    /// <param name="conditionName"></param>
    /// <param name="done"></param>
    public void SetCondition(string conditionName, bool done) {
        // Buscamos la condición en la lista de condiciones
        foreach(Condition condition in data.allCondition) {
            // Si la encontramos, cambiamos su valor
            if (condition.name == conditionName) {
                condition.done = done;
                return;
            }
        }

        Debug.LogWarning(conditionName + " - La condición no existe y no se puede cambiar");
    }




}

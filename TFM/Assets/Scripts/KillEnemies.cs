﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 06/10/2019
* Ultima fecha modificacion: 06/10/2019
* Descripcion: Script para comprobar que todos los enemigos hayan muerto y activar el panel que indique como desactivar los laseres

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEnemies : MonoBehaviour{

    // Array de enemigos a matar
    public GameObject[] enemies;

    // Interactable que activaremos al matar a todos los enemigos
    public GameObject interactableTrigger;

    // Booleana que activaremos al matar a todos los enemigos
    private bool enemiesDeath = false;

    // Contador interno de muertes
    private int contDeaths = 0;

    // Array de muertes
    //private bool[] enemyDead;

    private EnemyHealth[] enemyHealth;

    // Start is called before the first frame update
    void Start(){
        
        // Igualamos la longitud del array de muertes a la longitud de array de enemigos
        //enemyDead = new bool[enemies.Length];

        enemyHealth = new EnemyHealth[enemies.Length];

        for (int i = 0; i < enemies.Length; i++) {
            enemyHealth[i] = enemies[i].GetComponent<EnemyHealth>();
        }

        
    }

    // Update is called once per frame
    void Update(){

        // Llamamos al método de matar a todos los enemigos
        KillAllEnemies();

        // Si están muerto todos los enemigos, activamos el interactable
        if(enemiesDeath){
            interactableTrigger.SetActive(true);

            // Desactivamos este script como medida de seguridad para que no haya problemas.
            this.enabled = false;
        }
    }


    /// <summary>
    /// Método para comprobar que todos los enemigos hayan muerto
    /// </summary>
    private void KillAllEnemies() {

        
      if(enemyHealth[contDeaths].isDead) {
            contDeaths++;
        }

        /*
        // Recorremos el array de los enemigos
        for(int i = 0; i < enemyHealth.Length; i++) {
            if(enemyHealth[i].isDead) {
                contDeaths--;
            }
        }
        */


        
        
        // Si el contador de muertes es mayor o igual a la longitud del array, significará que hemos matado a todos los enemigos
        if (contDeaths >= enemies.Length) {
            // entonces indicamos que han muerto todos los enemigos
            enemiesDeath = true;
        }
        
            
        
    }
}

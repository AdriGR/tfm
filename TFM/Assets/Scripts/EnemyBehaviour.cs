﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 01/09/2019
* Ultima fecha modificacion: 07/09/2019
* Descripcion: Controla las acciones de los enemigos, cuando moverse y cuando cambiar de estado a ataque, patrulla y alerta.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Para el uso del Nav Mesh
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour {

    // Para definir cada uno de los posibles estados de nuestra IA
    // Patroll --> modo patrulla, de waypoint a waypoint
    // Noise   --> modo alerta, avanzando hacia la posición donde ha escuchado algo
    // Attack  --> modo ataque, inicia una persecución
    public enum States { Patroll, Noise, Attack }

    [Header("States")]
    // Estado actual de la IA
    public States state;
    // Referencia al TextMesh que mostrará el estado actual de la Ia
    //public TextMesh stateText;

    [Header("Patroll")]
    // Listado de waypoints elegibles para la patrulla
    public Transform[] waypoints;
    // Id del waypoint al que se dirige la IA
    public int actualWaypoint;
    // Distancia a la que se tiene que acercar al waypoint antes de pasar al siguiente 
    public float minDistanceChangeWaypoint = 0.2f;

    [Header("Attack")]
    // Alcance de lavisión para la detección de objetivos
    public float reach = 50f;
    // Transform del ojo que será usado como origen del raycast
    public Transform eye;
    // Layers que podrá ver el enemy
    public LayerMask raycastLayer;

    // Campo de visión del enemigo ( amplitud en grados a los que se moverá el raycast )
    public float fieldOfView = 90f;
    // Punto en el que se encontrará mirando el ojo
    private float fovCounter;
    // Velocidad de barrido del ojo
    public float turnSpeed = 3f;
    // Dirección de movimiento del ojo
    private bool viewDirectionRight = true; 
    // Objetivo avistado a perseguir
    private Transform target;
    // Indica si el objetivo está a la vista
    private bool targetAtView = false;

    // Tiempo para que la IA de por perdido al jugador 
    public float timeToDisengage = 10f;
    // Contador regresivo para saber cuando dejar de seguir al jugador
    public float disengageCounter;

    // Distancia mínima a la que el enemigo atacará
    public float minAttackRange = 7f;



    // Tiempo que necesita para volver a atacar
    public float timeToPunch = 3f;

    private float punchCounter;

    



    [Header("Noise")]
    // Distancia a la que podrá oir un ruido como máximo
    public float hearDistance = 10f;
    // Posición del último sonido detectado
    private Vector3 lastSoundPosition;

    // Referencia al componente Nav Mesh Agent
    private NavMeshAgent nav;

    public bool drawGizmo;


    // Referencia al audioSource
    private AudioSource audioSource;

    // Referencia al playerHealth para saber cuando está muerto
    private PlayerHealth playerHealth;

    // Use this for initialization
    void Start () {

        // Recuperamos la referencia al componente
        nav = GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();
        playerHealth = GameObject.Find("Player").GetComponent<PlayerHealth>();

        // Por defecto empezamos en modo patrulla
        state = States.Patroll;

        // Iniciamos el checkeo del estado en el que se encuentra la IA
        StartCoroutine(StateCheck());

        // Iniciamos el fovCounter
        fovCounter = fieldOfView / 2;
	}
	
	// Update is called once per frame
	void Update () {

        CheckView();

        if (state == States.Attack) {

            if (disengageCounter > 0) {

                disengageCounter -= Time.deltaTime;
            } else {

                state = States.Patroll;
            }

        }

        if (punchCounter <= 0) {
            punchCounter = timeToPunch;
        } else {
            punchCounter -= Time.deltaTime;
        }
        

	}

    /// <summary>
    /// Corrutina con bucle infinito con una actualización de tiempo inferior al Update
    /// Para gestionar los cambios de la máquina de estados de la IA
    /// </summary>
    /// <returns></returns>
    private IEnumerator StateCheck () {

        while (true) {

            // Ejecucción del método correspondiente según el estado actual
            if (state == States.Patroll) {

                Patroll();
            } else if ( state == States.Attack) {

                Attack();
            } else if ( state == States.Noise) {

                FollowNoise();
            }

            // Actualizo el indicador de estado
            //StateIndicator();

            yield return new WaitForSeconds(0.1f);
        }
    }

    /*
    /// <summary>
    /// Verificará el estado del indicador
    /// </summary>
    private void StateIndicator() {

        // Para que siempre sea legible este indicador, lo rotaremos en dirección a la cámara principal
        stateText.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);

        if (state == States.Patroll) {

            stateText.text = "";
        } else if (state == States.Attack) {

            stateText.color = Color.red;
            stateText.text = "!";
        } else if (state == States.Noise) {

            stateText.color = Color.yellow;
            stateText.text = "?";
        }
    }
    */

    /// <summary>
    /// Acciones de patrullar por los waypoints definidos
    /// </summary>
    private void Patroll () {

        // Indicamos que no estamos parados
        nav.isStopped = false;

        // Si la distancia al waypoint es inferior a la indicada
        if (nav.remainingDistance < minDistanceChangeWaypoint) {

            // Pasamos al siguiente waypoint
            actualWaypoint++;
            // Si hemos superado ya el número máximo de waypoints, volvemos al primero
            actualWaypoint = (actualWaypoint >= waypoints.Length) ? 0 : actualWaypoint;
        }

        // Fijamos la ruta hacia el waypoint correspondiente
        nav.SetDestination(waypoints[actualWaypoint].position);
    }

    /// <summary>
    /// Verifica la visión del enemigo
    /// </summary>
    private void CheckView() {

        // Destino de la visión del raycast
        Vector3 targetView = (-eye.forward * reach + transform.position);

        if (viewDirectionRight) {

            // 
            eye.localEulerAngles = new Vector3(eye.localEulerAngles.x, eye.localEulerAngles.y + turnSpeed * Time.deltaTime, eye.localEulerAngles.z);

            viewDirectionRight = (eye.localEulerAngles.y < (180 + fieldOfView / 2));
        } else {

            eye.localEulerAngles = new Vector3(eye.localEulerAngles.x, eye.localEulerAngles.y - turnSpeed * Time.deltaTime, eye.localEulerAngles.z);

            viewDirectionRight = !(eye.localEulerAngles.y > (180 - fieldOfView / 2));
        }

        // Variable para almacenar el resultado del impacto del raycast
        RaycastHit hit = new RaycastHit();

        // Realizamos un linecast entre el ojo y el punto de visión final
        if (Physics.Linecast(eye.position, targetView, out hit, raycastLayer)) {

            // Si el objeto impactado es el jugador, y no estamos en estado de ataque
            if (hit.collider.tag == "Player" && state != States.Attack) {

                Debug.Log("Jugador a la vista");

                // Cambiamos el estado a ataque
                state = States.Attack;
                // Indicamos que el objetivo de ataque será el jugador avistado
                target = hit.collider.transform;

                // Reseteo el contador
                disengageCounter = timeToDisengage;

                // Muestro un drawline de color rojo para indicar que la ia ha visto al jugador
                Debug.DrawLine(eye.position, hit.point, Color.red);
            }
            
            // Si el jugador es visto estando en modo ataque
            if (hit.collider.tag == "Player" && state == States.Attack) {

                // Reseteo el contador
                disengageCounter = timeToDisengage;

                // Muestro un drawline de color rojo para indicar que la AI ha visto al jugador
                Debug.DrawLine(eye.position, hit.point, Color.red);
            } else {

                // Muestro un drawline en color verde mientras no vea al jugador
                Debug.DrawLine(eye.position, hit.point, Color.green);
            }

            // Si detectamos al player, indicamos que está a la vista y reseteamos el tiempo de pérdida
            if (hit.collider.tag == "Player") {
                targetAtView = true;
                disengageCounter = timeToDisengage;
            } else {
                // Si no, indicamos qeu no está a la vista
                targetAtView = false;
            }
        } else {

            // Para en el caso en el que no colisione con ningún objeto
            Debug.DrawRay(eye.position, -eye.forward * reach, Color.green);
            
            targetAtView = false;
        }      
    }

    /// <summary>
    /// Acciones del modo ataque
    /// </summary>
    private void Attack() {
        // Actualizamos la posición del objetivo
        nav.SetDestination(target.position);

        // Si el player ha muerto, volvemos al estado de patrulla
        if(playerHealth.isDead) {
            state = States.Patroll;
        }

    }


    /// <summary>
    ///  Almacena la posición del último sonido escuchado e inicia el estado de seguimiento de sonido
    /// </summary>
    /// <param name="position"></param>
    public void HearSound(Vector3 position) {

        // Si el sonido se encuentra a más distancia de la que la IA puede escuchar
        if (Vector3.Distance(transform.position, position) > hearDistance) {

            return;
        }

        // Si nos encontramos en un estado distinto al Attack
        if ( state != States.Attack) {

            // Activamos el modo seguimiento de sonido
            state = States.Noise;
        }

        // Indicamos el origen del último sonido escuchado
        lastSoundPosition = position;
    }

    /// <summary>
    /// Sigue la posición del último sonido escuchado
    /// </summary>
    private void FollowNoise() {

        // Fijo el destino del desplazamiento definido
        nav.SetDestination(lastSoundPosition);

        // Si la distancia al destino definido, es inferior a la distancia mínima para cambiar de waypoint
        if(nav.remainingDistance < minDistanceChangeWaypoint) {

            // Paso nuevamente al modo patrulla
            state = States.Patroll;
        }
    }

    /// <summary>
    /// Método para reaccionar cuando recibe daño
    /// </summary>
    /// <param name="newTarget"></param>
    public void TakingDamage(Transform origin) {

        // Indicamos que el target será el origen del daño producido
        target = origin;
        // Cambiamos el estado al estado de ataque
        state = States.Attack;
        // Reseteo el contador
        disengageCounter = timeToDisengage;
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Player") {
            // Si objeto con el que colisiona tiene la etiqueta de player, detenemos el movimiento
            nav.isStopped = true;
        }
    }

    private void OnCollisionExit(Collision collision) {
        if (collision.gameObject.tag == "Player") {
            // Si el objeto con el que deja de colisionar tiene la etiqueta de player, indicamos que se vuelva a mover
            nav.isStopped = false;
        }
    }

    private void OnDrawGizmos() {

        if (!drawGizmo) {

            return;
        }

        Gizmos.DrawWireSphere(transform.position, hearDistance);
    }
}

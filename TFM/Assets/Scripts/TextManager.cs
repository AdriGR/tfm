﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 27/08/2019
* Última fecha modificación: 31/08/2019
* Descripción: Permite mostrar el texto de dialogos.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para usar elementos UI
using UnityEngine.UI;

public class TextManager : MonoBehaviour{

    // Referencia al panel donde apareceran los textos
    public GameObject textPanel;

    // Para saber si el texto ha terminado
    public bool finish = false;

    // Referencia al campo de texto
    public Text text;

    // Tecla a presionar para continuar el texto o con la siguiente acción
    public Image actionButtonImage;

    // Velocidad de mostrado del texto.
    public Slider speedText;
    //public float velocityText = 0.5f;


    // Tiempo mínimo de mostrado por caracter
    public float displayTimePerCharacter = 0.1f;

    // Tiempo de mostrado adicional
    public float additionalDisplayTime = 0.5f;

    // Longitud del texto
    private int lengthText;

    // Contador del array para saber por donde vamos
    private int contArray;



    public static TextManager instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start(){
        // Indicamos que al inicio empiece con el sprite de pulsar el botón desactivado
        actionButtonImage.enabled = false;

        

        
    }

    // Update is called once per frame
    void Update(){
        /*
        // Si el tiempo del sistema es superior al tiempo estimado de eliminación, llevamos a cabo la retirada de texto
        if (Time.time >= clearTime) {
            // Vaciamos el texto
            text.text = "";

            // Desactivamos el panel
            textPanel.SetActive(false);
        }
        */

        // Si pulsamos el botón derecho del ratón y la longitud del array del texto es mayor o igual a la longitud del texto
        if(Input.GetButtonDown("Interaction") && contArray >= lengthText) {

                // Si ha terminado
                finish = true;
                    // Desactivamos el panel
                    textPanel.SetActive(false);

                    // Vaciamos el texto que estuviera
                    text.text = "";

        }
        


    }

    /// <summary>
    /// Método que muestra el texto por pantalla
    /// </summary>
    /// <param name="message"></param>
    /// <param name="textColor"></param>
    public void DisplayMessage(string message) {

        // Iniciamos el texto indicando que no está completo
        finish = false;

        // guardamos la longitud del array
        lengthText = message.Length;

        // Activamos el panel
        textPanel.SetActive(true);

        // Calculamos la duración del texto en pantalla
        //float displayDuration = message.Length * displayTimePerCharacter + additionalDisplayTime;

        // Calculamos el momento en el que deberá retirarse el texto
        //clearTime = Time.time + displayDuration;

        //textChar = message.ToCharArray();

        // Empezamos la corrutina para darle velocidad de muestreo al texto
        StartCoroutine(VelocityText(message));

        // Empezamos la corrutina por si pulsamos el botón derecho del ratón para autocompletar el texto
        //StartCoroutine(CompleteText(message));







    }

    private IEnumerator VelocityText(string message) {
        
        // Recorremos el string
        for(contArray = 0; contArray < message.Length; contArray++) {


                // Indicamos la velocidad que tendrá de mostrado
                float displayDuration = speedText.value;

                // Esperamos los segundos indicados
                yield return new WaitForSeconds(displayDuration);

                if(contArray <=  lengthText - 1) {
                    // Asignamos cada letra al texto
                    text.text += message[contArray].ToString();
                }


                // Si el contador del array es mayor o igual que su longitud menos 1, habilitamos la imagen que indica que botón pulsar
                if(contArray >= message.Length - 1) {
                    actionButtonImage.enabled = true;
                    // Indicamos que el texto ha terminado
                    //finish = true;

                    // Paramos la corrutina
                    StopCoroutine(VelocityText(message));

                } else {
                    // Si no, lo dejamos inhabilitado
                    actionButtonImage.enabled = false;
                }

            // Si pulsamos el botón de interacción y el texto aún no ha terminado, llamamos al método que lo completa
            if(Input.GetButtonDown("Interaction") && contArray < message.Length) {
                CompleteText(message);
            }
        }

    }

    /// <summary>
    /// Método para completar el texto
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public string CompleteText(string message) {
        // Indicamos que el texto es igual al mensaje
        text.text = message.ToString();

        // Paramos la otra corrutina
        StopCoroutine(VelocityText(message));

        // Indicamos que el contador del array es igual a la longitud del mensaje
        contArray = message.Length;

        // Habilitamos la imagen del botón a pulsar para pasar a la siguiente reacción
        actionButtonImage.enabled = true;

        // Indicamos que ha terminado
        //finish = true;
        return text.text;
    }

    /*
    /// <summary>
    /// Corrutina para mostrar el texto entero y cancelar la anterior
    /// </summary>
    /// <returns></returns>
    private IEnumerator CompleteText(string message) {

        while(true) {
            // Si pulsamos el botón derecho y el contador del array es menor a la longitud del texto, mostramos el texto entero
            if(Input.GetButtonDown("Interaction") && contArray < lengthText) {

                

                // Indicamos que el texto es igual al mensaje
                text.text = message.ToString();

                // Paramos la otra corrutina
                StopCoroutine(VelocityText(message));

                // Indicamos que el contador del array es igual a la longitud del mensaje
                contArray = message.Length;

                // Habilitamos la imagen del botón a pulsar para pasar a la siguiente reacción
                actionButtonImage.enabled = true;

                // Indicamos que ha terminado
                //finish = true;

                
            } else {
                // Si no, no devolvemos nada
                yield return null;
            }

        }
    }*/

}

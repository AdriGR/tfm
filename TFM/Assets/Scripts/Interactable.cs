﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 12/05/2019
* Última fecha modificación: 23/09/2019
* Descripción: Permite ejecutar las reacciones de los interactables

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour{

    // Transform que indica la posición y la dirección a la que el jugador mirará al realizar la interacción
    public Transform interactionLocation;


    [Header("Condiciones")]
    // Array de condiciones
    public string[] conditions;

    // Reacciones positivas y por defectos
    public Transform positiveReactions;
    public Transform defaultReactions;

    // Referencia al player
    private GameObject player;

    protected virtual void Start() {
        // Buscamos la referencia al player controller y recuperamos su componente
        player = GameObject.FindGameObjectWithTag("Player");
    }


    private void Update() {
        
    }


    /// <summary>
    /// Método que gestionará las condiciones y las reacciones
    /// </summary>
    public void Interact() {
        // Indicamos que el player mire hacia el objeto interactuado
        player.transform.rotation = Quaternion.LookRotation(transform.position);

        // Por defecto marcamos como cumplidas las condiciones
        bool success = true;

        // Recorro todas las condiciones del interactable
        foreach (string condition in conditions) {
            // Compruebo si no se cumple la condición
            if(!DataManager.instance.CheckCondition(condition)) {
                // Almaceno en la variable si no se cumple
                success = false;
            }
        }

        // Si se cumplen las condiciones y el número de condiciones es mayor a 0
        if (success && conditions.Length > 0) {

            // Recuperamos todos los componentes de tipo reaction que sean hijos del objeto vacío positive reactions
            Reaction[] posReact = positiveReactions.GetComponentsInChildren<Reaction>();
            // Ejecutamos todas las reacciones
            //foreach (Reaction react in posReact) {
            //react.ExecuteReaction();

            // Ejecutamos la primera reacción
            posReact[0].ExecuteReaction();
            //}
        } else {

            // Recuperamos todos los componentes de tipo reaction que sean hijos del objeto vacío default reactions
            Reaction[] defReact = defaultReactions.GetComponentsInChildren<Reaction>();

            defReact[0].ExecuteReaction();
            /*
            // Ejecutamos todas las reacciones
            foreach(Reaction react in defReact) {
                react.ExecuteReaction();
            }
            */   
        }
    }

   
}

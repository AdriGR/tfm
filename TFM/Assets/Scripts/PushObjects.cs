﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushObjects : MonoBehaviour
{
    // Fuerza de empuje de objetos.
    public float pushStrength = 6.0f;

    //Velocidad de empuje, que será la misma que la que tiene el player.
    private float tpc = 5;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {

    }
    /// <summary>
    /// Cuando el jugador golpee un objeto lo empuja
    /// </summary>
    /// <param name="hit"></param>
    private void OnControllerColliderHit(ControllerColliderHit hit) {

        Rigidbody body = hit.collider.attachedRigidbody;

        if (body == null || body.isKinematic) {
            return;
        }

        if (hit.moveDirection.y < -0.3f)
        {
            return;
        }
        pushStrength = tpc;

        Vector3 direction = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
        body.velocity = direction * pushStrength;




    }
}

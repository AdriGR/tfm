﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 25/09/2019
* Ultima fecha modificacion: 25/09/2019
* Descripcion: Controla el puzzle de luces que se interactuen en el orden correcto y activa la ultima 
* esfera interactable para poner la luz bien.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleLight : MonoBehaviour{

    public bool success = false;

    // Variable que usaremos para guardar el color de la luz direccional antes de que cambiara.
    public Color initColor;

    // Color del jugador que cambiaremos para indicar daño
    public Color damageColor;

    // Daño que hará al jugador mientras no se resuelva el puzzle
    public float damageLight;

    // Velocidad a la que irá cambiando el color
    public float speedColor = 0.01f;

    // Referencia a la luz direccional
    private Light directionalLight;

    // Referencia a la vida del jugador
    private PlayerHealth playerHealth;

    // Referencia al material del player
    private Renderer playerRenderer;

    // Referencia al componente audioSource
    private AudioSource audioSource;


    // Start is called before the first frame update
    void Start(){
        
        // Recuperamos componente de la luz y audioSource
        directionalLight = GameObject.Find("Directional Light").GetComponent<Light>();
        audioSource = GetComponent<AudioSource>();

        // Recuperamos el componente de vida del player y su renderer en el hijo que es donde está su material
        playerHealth = GameObject.Find("Player").GetComponent<PlayerHealth>();
        playerRenderer = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Renderer>();

        // Si la condición del puzzle de luz no está activada, significa que no lo hemos resuelto
        if(!DataManager.instance.CheckCondition("PuzzleLightClue")) {
            // Indicamos que no está realizado y la luz la ponemos en rojo
            success = false;
            directionalLight.color = Color.red;

            // Reproducimos el sonido
            audioSource.Play();
        } else {
            // Si no indico que está resuelto
            success = true;
        }

        
    }

    // Update is called once per frame
    void Update(){
        if (!success) {
            // Mientras no completemos el puzzle, restamos vida al jugador
            playerHealth.TakeDamagePuzzle(damageLight * Time.deltaTime);

            // Cambiamos el color del renderer de su color normal a verde y viceversa a la velocidad indicada
            playerRenderer.material.SetColor("_Color", Color.Lerp(Color.white, damageColor, Mathf.PingPong(speedColor * Time.time, 1)));
        }
    }

    /// <summary>
    /// Método para indicar que se ha resuelto el puzzle
    /// </summary>
    public void SuccessfulPuzzle() {

        // Indicamos que el puzzle ha sido completado
        success = true;

        // Cambiamos el color de la luz direccional al que estaba al principio
        directionalLight.color = initColor;

        // Paramos el sonido
        audioSource.Stop();

        // Ponemos el color del personaje de nuevo en blanco
        playerRenderer.material.SetColor("_Color", Color.white);
    }

    
    
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 26/09/2019
* Ultima fecha modificacion: 26/09/2019
* Descripcion: Interactable que se ejecutará por trigger

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableTrigger : Interactable{

    private void OnTriggerEnter(Collider other) {
        // Si entra en contacto con otro objeto cuya etiqueta sea player
        if (other.CompareTag("Player")) {
            // Ejecutamos el método de interactuar
            Interact();
        }
    }
}

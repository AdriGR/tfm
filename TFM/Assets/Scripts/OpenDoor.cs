﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {

    public GameObject door;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    private void OnTriggerEnter(Collider other) {

        if (other.CompareTag("PushableCube")) {

            door.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other) {

        if (other.CompareTag("PushableCube")) {

            door.SetActive(true);
        }
    }
}

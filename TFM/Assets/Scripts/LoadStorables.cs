﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 
* Ultima fecha modificacion:
* Descripcion:

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadStorables : MonoBehaviour{

    // Referencia al transform de storables que contiene los objetos a guardar
    private Transform storables;

    // Contador de objetos que contiene el gameObject storable
    private int storablesObjects;

    // Start is called before the first frame update
    void Start(){
        // Buscamos el objeto storable
        storables = GameObject.Find("Storables").transform;

        // Guardamos los hijos que tenga el objeto storables
        storablesObjects = storables.childCount;

        // Cargamos la información
        LoadStorages();
    }

    // Update is called once per frame
    void Update(){
        
    }

    /// <summary>
    /// Método para cargar los valores de los objetos guardables que llamaremos desde el scene Controller una vez que la escena este cargada, justo antes de hacer fade In
    /// </summary>
    public void LoadStorages() {

        if(DataManager.instance.data.storable.Count > 0) {
            // Recorremos los hijos del transform storables
            for(int i = 0; i <= storablesObjects - 1; i++) {
                // Comprobamos si el nombre coincide con el que está guardado, de ser así, cargamos sus valores
                if(storables.GetChild(i).name == DataManager.instance.data.storable[i].name) {
                    storables.GetChild(i).transform.position = new Vector3(DataManager.instance.data.storable[i].objectPositionX,   // Recuperamos su posiicón en X
                                                                           DataManager.instance.data.storable[i].objectPositionY,   // Recuperamos su posición en Y
                                                                           DataManager.instance.data.storable[i].objectPositionZ);  // Recuperamos su posición en Z
                    // Recuperamos su estado
                    storables.GetChild(i).gameObject.SetActive(DataManager.instance.data.storable[i].state);
                }
            }
        }



    }
}

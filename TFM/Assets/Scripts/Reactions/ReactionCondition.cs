﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionCondition : Reaction{

    // Nombre de la condición a cumplir
    public string conditionName;

    // Estado al que cambiará la condición
    public bool conditionStatus;

    protected override IEnumerator React() {
        yield return base.React();

        // Modificamos el estado de la condición
        DataManager.instance.SetCondition(conditionName, conditionStatus);

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 24/08/2019
* Última fecha modificación: 28/08/2019
* Descripción: Permite Activar o desactivar objetos en las reacciones.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionObjectActivation : Reaction{

    // Objeto a activar/desactivar
    public GameObject targetObject;

    // Estado en el que quedará el objeto
    public bool active;


    /// <summary>
    /// Override del método heredado
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator React() {
        yield return base.React();

        // Modificamos el estado del objeto en función de la opción elegida
        targetObject.SetActive(active);

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

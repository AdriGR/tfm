﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 11/10/2019
* Ultima fecha modificacion: 11/10/2019 
* Descripcion: Reacción para mostrar paneles informativos y parar el tiempo hasta que se pulse la tecla indicada y se cierre

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReactionActivePanel : Reaction{

    // Panel a mostrar
    public GameObject panel;



    // Update is called once per frame
    void Update(){
        // Si pulsamos el botón de interacción, desactivamos el panel
        if(Input.GetButtonDown("Interaction")) {
            panel.SetActive(false);
        }
    }


    protected override IEnumerator React() {
        // Ejecutamos el método heredado
        yield return base.React();

        // Activamos el panel
        panel.SetActive(true);

        // Paramos el tiempo
        Time.timeScale = 0f;

        // Esperamos hasta que el panel se cierre
        yield return new WaitUntil(() => !panel.activeSelf);

        // Activamos el tiempo
        Time.timeScale = 1f;

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

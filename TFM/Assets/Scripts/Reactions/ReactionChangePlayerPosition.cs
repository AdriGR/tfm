﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 26/09/2019
* Ultima fecha modificacion: 26/09/2019
* Descripcion: Reaccion para cambiar al jugador de posición

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionChangePlayerPosition : Reaction{

    // Referencia al jugador
    private Transform player;

    // Posición de destino
    public Transform changePosition;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos al player
        player = GameObject.Find("Player").transform;
    }

    protected override IEnumerator React() {
        yield return base.React();

        // Cambiamos al jugador a la posición de destino
        player.position = changePosition.position;

        // Ejecutamos la siguiente reaccion
        nextReaction.ExecuteReaction();
    }
}

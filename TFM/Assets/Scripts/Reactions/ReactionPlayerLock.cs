﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 01/09/2019
* Ultima fecha modificacion: 23/09/2019
* Descripcion: Bloquea al jugador para que no se mueva mientras haya reacciones por hacer

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionPlayerLock : Reaction{

    // Referencia al player
    private PlayerController playerController;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos la referencia al playerController
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    protected override IEnumerator React() {

        // Realizamos la espera, en este caso la pulsación de una tecla
        yield return base.React();

        // Desabilitamos el componente para que no se pueda mover
        playerController.enabled = false;
        
        // Realizamos el bloqueo de las interacciones
        playerController.blockMove = true;

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }

    private void OnDisable() {
        // Si se desactivara el objeto con la interacción, devuelvo el control al jugador
        playerController.enabled = true;
    }
}

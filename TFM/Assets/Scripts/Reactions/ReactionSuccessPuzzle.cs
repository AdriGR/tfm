﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 26/09/2019
* Ultima fecha modificacion: 26/09/2019
* Descripcion: Reacción para completar los puzzles

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionSuccessPuzzle : Reaction{

    // Objeto que tendrá el método a ejecutar
    public PuzzleLight target;

    protected override IEnumerator React() {
        yield return base.React();

        // Ejecutamos el método para resolver el puzzle
        target.GetComponent<PuzzleLight>().SuccessfulPuzzle();
        

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }


}

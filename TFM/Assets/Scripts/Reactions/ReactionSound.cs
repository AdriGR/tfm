﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 24/08/2019
* Última fecha modificación: 28/08/2019
* Descripción: Permite hacer las reacciones para el sonido.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionSound : Reaction{


    // Sonido a reproducir
    public AudioClip audioClip;

    // Referencia al audioSource
    private AudioSource audioSource;

    void Start() {
        // Recuperamos el componente
        audioSource = GetComponent<AudioSource>();    
    }


    /// <summary>
    /// Override del método heredado
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator React() {

        // Ejecutamos el contenido del método heredado
        yield return base.React();

        // Le asignamos el clip de audio a reproducir
        audioSource.clip = audioClip;

        // Reproducimos el clip de audio
        audioSource.Play();

        // Esperamos a que el audio se reproduzca
        yield return new WaitForSeconds(audioSource.clip.length); 



        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }

}

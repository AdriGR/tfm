﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionNextReactions : Reaction{


    // Reacciones que se ejecutarán al terminar la anterior
    public Reaction[] nextReactions;

    protected override IEnumerator React() {
        yield return base.React();

        // Recorremos todas las reacciones que haya en el array
        for (int i = 0; i < nextReactions.Length; i++) {
            // Las ejecutamos
            nextReactions[i].ExecuteReaction();
        }
        
    }
}

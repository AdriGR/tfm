﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 04/10/219 
* Ultima fecha modificacion: 04/10/2019
* Descripcion: Reacción para instanciar enemigos

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionInstantiateEnemies : Reaction{

    // Transform donde se instanciaran
    public Transform[] positionsEnemies;

    // Start is called before the first frame update
    void Start(){
        
    }

    protected override IEnumerator React() {
        yield return base.React();

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

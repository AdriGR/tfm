﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 11/10/2019
* Ultima fecha modificacion: 11/10/2019
* Descripcion: Cambia el color del material

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionChangeColor : Reaction{

    // Color a cambiar
    public Color changeColor;

    // Objeto al cual se le cambiará el material
    public GameObject obj;

    private Renderer rend;

    // Start is called before the first frame update
    void Start(){
        rend = obj.GetComponent<Renderer>();
    }

    protected override IEnumerator React() {
        // Ejecutamos el método heredado
        yield return base.React();

        // Cambiamos el color del material
        rend.material.SetColor("_Color", changeColor);

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 21/08/2019
* Última fecha modificación: 28/08/2019
* Descripción: Permite mostrar el texto de dialogos.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionAnimation : Reaction{

    // Objetivo que será animado
    public GameObject target;

    // Nombre del trigger del animator a disparar
    public string triggerName;


    /// <summary>
    /// Método que ejecuta la reacción, con override para que pise la corrutina heredada
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator React() {
        yield return new WaitForSeconds(0f);
       
        // Obtenemos el componente animator del objeto y ejecutamos su animación trigger.
        target.GetComponent<Animator>().SetTrigger(triggerName);

        

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

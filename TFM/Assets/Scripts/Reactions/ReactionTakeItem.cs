﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 04/09/2019    
* Ultima fecha modificacion: 04/09/2019
* Descripcion: Reacción que permite agregar el objeto al inventario

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionTakeItem : Reaction{

    // Variable para almacenar la referencia del item activo/actual
    private InteractableItem actualItem;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos el componente en el padre
        actualItem = GetComponentInParent<InteractableItem>();
    }

    // Update is called once per frame
    void Update(){
        
    }


    /// <summary>
    /// Método que realiza las acciones necesarias para recoger el item y mostrarlo en el inventario
    /// </summary>
    private void PickUpItem() {
        // Recorremos el listado de todos los items disponibles
        foreach(Item item in DataManager.instance.data.allItem) {
            // Si coincide con el nombre del item actual
            if(item.name == actualItem.nameItem) {
                // Indicamos al inventory manager que agregue al inventario el objeto actual
                if(InventoryManager.instance.AddItemInventory(actualItem.nameItem)) {
                    // Dejamos indicado en el listado de items que se ha recogido
                    item.picked = true;

                    // Desactivamos el objeto de la escena
                    actualItem.gameObject.SetActive(false);
                } else {
                    Debug.Log("No se puede añadir el objeto porque falta algo");
                }

                // Si ya hemos encontrado el objeto hacemos una salida del método para terminar el bucle
                return;
            }

        }

        // Si llegamos aquí, nos avisamos de que algo ha fallado
        Debug.LogWarning("El nombre del objeto no existe: " + actualItem.name);
    }


    /// <summary>
    /// Método heredado que ejecuta la reaccion
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator React() {
        yield return base.React();

        // Llamamos al método para recoger el objeto
        PickUpItem();
    }
}

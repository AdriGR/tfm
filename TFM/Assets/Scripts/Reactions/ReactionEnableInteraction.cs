﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 23/09/2019
* Ultima fecha modificacion: 23/09/2019
* Descripcion: Activa la interacción del objeto

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionEnableInteraction : Reaction{

    // Referencia al box collider que hace de interactable
    private Collider interactableCollider;

    // Referencia al botón de interacción
    private InputButton inputButton;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componentes
        interactableCollider = GetComponentInParent<Collider>();
        inputButton = GetComponentInParent<InputButton>();
    }

    protected override IEnumerator React() {
        // Realizamos la espera
       yield return base.React();

        // Activamos el componente
        interactableCollider.enabled = true;

        // Activamos de nuevo la booleana para que pueda interactuar y activamos la imagen del botón
        inputButton.canInteract = true;
        inputButton.interactButton.SetActive(true);

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 28/08/2019
* Ultima fecha modificacion: 23/09/2019
* Descripcion: Permite mostrar los textos en las reacciones

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionText : Reaction{

    // Texto a mostrar
    public string textID;




    /// <summary>
    ///  Override del método heredado
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator React() {


        // Llamamos al text manager pasandole la cadena de texto traducida para que la muestre
        TextManager.instance.DisplayMessage(TranslateManager.instance.GetString(textID));

        // Ocultamos el inventario
        InventoryManager.instance.inventoryCanvas.SetActive(false);

        // Esperamos hasta que el panel esté desactivado para pasar a la siguiente interacción
        yield return new WaitUntil (() => !TextManager.instance.textPanel.activeInHierarchy);

        // Activamos el inventario
        InventoryManager.instance.inventoryCanvas.SetActive(true);

            // Ejecutamos el código heredado
            yield return base.React();

            // Ejecutamos la siguiente reacción
            nextReaction.ExecuteReaction();
        
        

        




    }

    /*
    /// <summary>
    /// Método para indicar a waitUntil si debe esperar o no
    /// </summary>
    /// <returns></returns>
    private bool WaitInteraction() {
        if (Input.GetButtonDown("Interaction")) {
            return true;
        } else {
            return false;
        }
    }
    */
}

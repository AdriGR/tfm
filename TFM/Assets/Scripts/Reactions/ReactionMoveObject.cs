﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 08/10/2019    
* Ultima fecha modificacion: 08/10/2019
* Descripcion: Reacción para mover los objetos a un lugar indicado

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionMoveObject : Reaction{

    // Objeto a mover
    public GameObject targetObject;

    // Transform donde moveremos el objeto
    public Transform targetPosition;

    // Booleana para indicar cuando ha llegado al destino
    private bool objectInPosition = false;

    // Booleana para indicar cuando podremos mover el objeto
    private bool moveObject = false;

    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update(){

        if(moveObject) {
            // Si podemos mover el objeto, lo movemos
            targetObject.transform.position = Vector3.MoveTowards(targetObject.transform.position, targetPosition.position, Time.deltaTime);
        }
        

        // Si el objeto está en la posición final, indicamos que ha llegado al destino
        if (targetObject.transform.position == targetPosition.position) {
            objectInPosition = true;

            // Una vez haya llegado al destino, ya no se podrá mover
            moveObject = false;
        }
    }


    protected override IEnumerator React() {
        // Ejecutamos el método heredado
        yield return base.React();

        // Indicamos que el objeto se puede mover
        moveObject = true;

        // Esperamos a que el objeto esté situado en el lugar correcto
        yield return new WaitUntil(() => objectInPosition);

        // Indicamos que no está en posición para la siguiente interacción que haya
        objectInPosition = false;

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();

        
    }
}

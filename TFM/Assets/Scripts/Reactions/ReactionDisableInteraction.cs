﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 23/09/2019
* Ultima fecha modificacion: 23/09/2019
* Descripcion: Desactiva la interacción del objeto

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionDisableInteraction : Reaction{

    // Referencia al Box collider trigger que hace de interactuable
    private Collider interactableCollider;

    // Referencia al interactable para deshabilitar la interacción
    private InputButton inputButton;
    


    // Start is called before the first frame update
    void Start(){
        // Recuperamos el componente
        interactableCollider = GetComponentInParent<Collider>();
        inputButton = GetComponentInParent<InputButton>();
    }

    protected override IEnumerator React() {

        // Realizamos la espera
        yield return base.React();

        // Deshabilitamos el collider
        interactableCollider.enabled = false;

        // Desactivamos la booleana que permite la interacción y desactivamos la imagen del botón a pulsar
        inputButton.canInteract = false;
        inputButton.interactButton.SetActive(false);


        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 24/09/2019
* Ultima fecha modificacion: 24/09/2019
* Descripcion: Reacción para cambiar el color de la luz direccional

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionChangeDirectionalLight : Reaction{

    // Referencia a la luz direccional
    private Light directionalLight;

    // Color al cual cambiaremos la luz
    public Color colorLight;

    // Start is called before the first frame update
    void Start(){
        //Recuperamos componente
        directionalLight = GameObject.Find("Directional Light").GetComponent<Light>();
    }


    protected override IEnumerator React() {
        yield return base.React();

        // Cambiamos el color de la luz
        directionalLight.color = colorLight;

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

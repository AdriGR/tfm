﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 11/10/2019
* Ultima fecha modificacion: 11/10/2019
* Descripcion: Reacción para habilitar/deshabilitar colliders de objetos

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionActivatorCollider : Reaction{

    // Booleana que indicará el estado del collider
    public bool isActive;

    // Objeto al cual se le cambiará el estado del collider
    public GameObject targetCollider;

    private Collider collider;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componente
        collider = targetCollider.GetComponent<Collider>();
    }

    protected override IEnumerator React() {
        // Ejecutamos el método heredado
        yield return base.React();

        // Activaremos o no el collider en función de la booleana
        collider.enabled = isActive;

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

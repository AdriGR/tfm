﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 11/10/2019
* Ultima fecha modificacion: 11/10/2019
* Descripcion: Reacción para parar o reanudar el movimiento de los enemigos.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ReactionActivatedEnemies : Reaction{

    // Booleana para saber en que estado quedará el componente
    public bool isActive;

    // Array interno que usaremos para detectar a todos los enemigos
    private Transform enemies;

    void Start() {
        enemies = GameObject.Find("Enemies").transform;    
    }




    protected override IEnumerator React() {
        // Ejecuramos el método heredado
        yield return base.React();

        // Recorremos todo el array de enemigos
        foreach(Transform enemy in enemies) {
            // Recuperamos su componente EnemyBehaviour
            EnemyBehaviour enemyBehaviour = enemy.GetComponent<EnemyBehaviour>();

            // Recuperamos su componente nav mesh
            NavMeshAgent nav = enemy.GetComponent<NavMeshAgent>();

            // Recuperamos su componente Animator
            Animator animator = enemy.GetComponent<Animator>();
            

            // Los activamos/desactivamos según el estado de la booleana
            enemyBehaviour.enabled = isActive;
            nav.isStopped = !isActive;
            animator.enabled = isActive;

        }


        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 02/10/2019
* Ultima fecha modificacion: 02/10/2019
* Descripcion: Realiza la oclusión de la cámara segun el estado en el que se encuentra la booleana

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionOcclusion : Reaction{

    // estádo que indicará si se realizará o no la oclusión
    public bool isOcclusion;


    protected override IEnumerator React() {

        // Indicamos al escene controller en que estado vamos a dejar la oclusión
        SceneController.instance.isOcclusion = isOcclusion;

        // Realizamos la espera del método heredado
        yield return base.React();

        // Ejecutamos la animación
        SceneController.instance.AnimationOcclusion();


        // Llamamos a la siguiente reacción
        nextReaction.ExecuteReaction();
    }
}

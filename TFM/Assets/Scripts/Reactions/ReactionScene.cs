﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 13/12/2019
* Ultima fecha modificacion: 13/12/2019
* Descripcion: Reacción para cambiar de escena

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionScene : Reaction{

    // nombre de la escena a la que cambiar
    public string nextScene;

    // Ejecutamos el método heredado
    protected override IEnumerator React() {
        yield return base.React();

        SceneController.instance.ChangeScenes(nextScene);
    }
}

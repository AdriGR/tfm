﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 12/05/2019
* Última fecha modificación: 23/09/2019
* Descripción: Permite mostrar el texto de dialogos.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Reaction : MonoBehaviour{

    // Descripción de la reacción, como una nota para el editor
    public string description;

    // Tiempo que tardará en ejecutarse esta acción
    public float delay;

    // Siguiente reacción a ejecutar
    public Reaction nextReaction;

    // Para saber si venimos de una reacción de texto
    public bool isText;


    /// <summary>
    /// Método genérico que ejecutará la reacción, será extendido a todas las clases que lo hereden
    /// </summary>
    public virtual void ExecuteReaction() {
        StartCoroutine(React());
    }

    /// <summary>
    /// Corrutina que será ejecutada como reacción
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator React() {
        // Si estamos en una reacción de texto y ha terminado, esperamos la pulsación de la tecla para seguir
        /*if (isText && TextManager.instance.finish) {
            yield return new WaitUntil(WaitInteraction);

            // Ejecutamos la siguiente reacción
            nextReaction.ExecuteReaction();

            // Si no, si no estamos en una reacción de texto y la siguiente reacción no esta vacía, esperamos los segundos indicados
        } else 
        */
        if  (nextReaction != null) {
            yield return new WaitForSeconds(delay);

        }
            

        

    }

    
}

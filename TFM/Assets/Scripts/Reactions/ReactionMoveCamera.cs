﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 07/10/2019
* Ultima fecha modificacion: 07/10/2019
* Descripcion: Reacción para mover la cámara a otros puntos especificados

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionMoveCamera : Reaction{

    // Referencia a la cámara
    public GameObject cam;

    // Transform de destino donde dirigiremos la cámara
    public Transform finalCamPosition;

    // Velocidad a la que desplazaremos la cámara
    public float camSpeed;

    // Booleana para saber si la cámara va hacia el objetivo o debe ir de vuelta al player
    public bool isTarget;

    // Booleana para indicar cuando ha llegado al destino
    private bool camInPosition = false;

    // Booleana para indicar cuando se moverá la cámara
    private bool startMoveCam = false;

    // Referencia al script de la cámara
    private Camera_track cameraTrack;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componente
        cameraTrack = cam.GetComponent<Camera_track>();



    }

    // Update is called once per frame
    void Update(){
        // Si indicamos que la cámara se mueva
        if(startMoveCam) {
            // Desactivamos el script para que la cámara no siga al player
            cameraTrack.enabled = false;

            // Desplazamos la cámara hasta el lugar indicado
            cam.transform.position = Vector3.MoveTowards(cam.transform.position, finalCamPosition.position, camSpeed * Time.deltaTime);
        }

        // Si la posición de la cámara es igual a la posición de destino, indicamos que está en posición
        if (cam.transform.position == finalCamPosition.position) {
            camInPosition = true;

            // Indicamos que la cámara no se mueva
            startMoveCam = false;

        }
        
    }

    protected override IEnumerator React() {
        // Ejecutamos el método heredado
        yield return base.React();

        // Si no se dirige al objetivo principal, indicamos que la posición final es la anterior al movimiento
        if(!isTarget) {
            finalCamPosition.position = cameraTrack.startReactionPosition;
        }

        // Indicamos que se empiece a mover la cámara
        startMoveCam = true;

        // Esperamos a que la cámara se haya posicionado
        yield return new WaitUntil(() => camInPosition);

        // Indicamos que la posición ya no es válida para la próxima interacción que se realice
        camInPosition = false;

        // Si va de vuelta al player, activamos el script para que vuelva a seguir al jugador
        if(!isTarget) {
            cameraTrack.enabled = true;
        }

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
        
    }


}

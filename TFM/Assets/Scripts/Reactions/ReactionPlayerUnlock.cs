﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 23/09/2019
* Ultima fecha modificacion: 23/09/2019
* Descripcion: Desbloquea al player para que pueda moverse

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionPlayerUnlock : Reaction{

    // Referencia al player controller
    private PlayerController playerController;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componentes
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    protected override IEnumerator React() {

        // Realizamos la espera
        yield return base.React();

        // Activamos el componente para que se pueda mover
        playerController.enabled = true;

        // Le habilitamos la interacción
        playerController.blockMove = false;



        
    }
}

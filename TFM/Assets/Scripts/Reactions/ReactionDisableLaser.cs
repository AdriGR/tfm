﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 11/10/2019
* Ultima fecha modificacion: 11/10/2019
* Descripcion: Reacción para quitar las barreras laser que impiden el paso

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionDisableLaser : Reaction{

    public GameObject[] laserBars;

    // Velocidad a la que se reducirá la escala de los objetos
    public float speed;

    // Booleana para indicar cuando debe empezar
    private bool startScale = false;

    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update(){
        // Si empieza a escalarse, reducimos la escala hasta que desaparezca
        if(startScale) {
            ReduceScale();
        }
    }

    protected override IEnumerator React() {
        // Ejecutamos el método heredado
        yield return base.React();

        // Indicamos que la escala se puede empezar a reducir
        startScale = true;

        // Ejecutamos la siguiente reacción
        nextReaction.ExecuteReaction();
        
    }

    /// <summary>
    /// Método para reducir la escala
    /// </summary>
    private void ReduceScale() {
        // Recorremos el array
        foreach(GameObject laserBar in laserBars) {
            // Reducimos su escala
            laserBar.transform.localScale -= new Vector3(speed * Time.deltaTime,speed * Time.deltaTime,speed * Time.deltaTime); 

            // Si la escala llega a 0, paramos la reducción
            if (laserBar.transform.localScale.x <= 0 && laserBar.transform.localScale.y <= 0f && laserBar.transform.localScale.z <= 0) {
                startScale = false;

                // Los desactivamos
                laserBar.SetActive(false);
            }
        }

        
    }
}

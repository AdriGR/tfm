﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 
* Ultima fecha modificacion:
* Descripcion:

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowBridge : MonoBehaviour{

    // Color del material a cambiar
    private Color fadeColor = new Color(1f, 1f, 1f, 0.1f);

    // Color del material al salir del trigger
    private Color transparentColor = new Color(1f, 1f, 1f, 0f);

    // Referncia al material
    private Renderer rend;

    void Start() {
        // Recuperamos el material
        rend = GetComponent<Renderer>();
    }


    private void OnTriggerEnter(Collider other) {
        // Si el otro collider que entra en contacto tiene la etiqueta player, activamos el objeto
        if(other.CompareTag("Player")){
            rend.material.color = fadeColor;
        }
    }

    private void OnTriggerExit(Collider other) {
        // Si el otro collider que sale del contacto tiene la etiqueta player, desactivamos el objeto
        if(other.CompareTag("Player")) {
            rend.material.color = transparentColor;
        }
    }
}

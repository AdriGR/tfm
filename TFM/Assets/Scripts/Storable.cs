﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 06/11/2019
* Ultima fecha modificacion: 06/11/2019
* Descripcion: Fichero serializable para poder cargar y guardar la posición, nombre y estado de los objetos.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Storable{
    
    // Nombre del objeto
    public string name;

    // Posición del objeto
    public float objectPositionX;
    public float objectPositionY;
    public float objectPositionZ;

    // Estado del objeto
    public bool state;
}

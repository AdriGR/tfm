﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 17/10/2019
* Ultima fecha modificacion: 17/10/2019
* Descripcion: Script para la bomba paralizadora

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalizedBomb : MonoBehaviour{

    // radio de la esfera de detección de daño
    public float explosionRadius = 3f;
    // daño aplicado con la explosión
    public int damage = 100;

    // Fuerza de lanzamiento de la bomba
    public float force;

    //muestra de forma visual el área que cubrirá la explosión
    public bool drawGizmo = true;
    // referencia al sistema de partículas que representará la explosión
    public ParticleSystem explosionParticles;

    // referencia al sistema de partículas que representará el trail de la bomba
    //public ParticleSystem trailParticles;

    // Referencia al rigidbody
    private Rigidbody rB;

    // Referencia al capsule collider
    private CapsuleCollider capsuleCollider;

    // para controlar  que solamente se active la explosión una vez
    private bool activated;

    void Start() {
        // Recuperamos componente
        rB = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
    }


    public void CallAttack() {
        // Recuperamos el rigidbody de la bomba y le añadimos fuerza hacia adelante
        //bulletRB.AddForce(transform.forward * bulletForce, ForceMode.Impulse);
        rB.useGravity = true;
        rB.isKinematic = false;

        // La desemparentamos 
        transform.SetParent(null);

        // Activamos el collider
        capsuleCollider.enabled = true;
    }

    private void OnTriggerEnter(Collider other) {
        // si la bala ya ha sido activada, no hacemos nada
        if(activated) {
            return;
        }

        // indicamos que la bala ha sido activada para que vuelva a producir el efecto.
        activated = true;

        // mostramos el efecto de partículas de la explosión
        explosionParticles.Play();

        // recuperamos todas las colisiones detectadas dentro del área de explosión calculada mediante una esfera
        Collider[] colls = Physics.OverlapSphere(transform.position, explosionRadius);
        // recorremos todas las colisiones detectadas
        foreach(Collider col in colls) {
            // intentamos recuperar el componente jaggernaut del objeto colisionado
            JaggernautBehaviour jaggernautBehaviour = col.GetComponent<JaggernautBehaviour>();

            // si el objeto colisionado tiene el componente jaggernautBehaviour
            if(jaggernautBehaviour != null) {
                // paralizamos al enemigo
                jaggernautBehaviour.EnemyParalized();
            }
        }
        // recuperamos todos los meshRendrer de los hijos de la bala
        MeshRenderer[] childrenMR = GetComponentsInChildren<MeshRenderer>();                                        //transform.localScale = Vector3.zero; otra forma de hacerlo mas facil reduciendo la escala
        foreach(MeshRenderer mr in childrenMR) {
            // desactivamos los mesh renderer para hacerlos invisibles
            mr.enabled = false;
        }
        // realizamos una parada de emisión del sistema de partículas
        //trailParticles.Stop();
        // programamos la destrucción de la bala para cuando haya terminado de reproducirse las partículas de exposión
        //Destroy(gameObject, explosionParticles.main.duration);
    }

    private void OnDrawGizmos() {
        if(!drawGizmo) {
            return;
        }
        // dibujamos el área de explosión de la bala
        Gizmos.DrawSphere(transform.position, explosionRadius);
    }
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 18/09/2019
* Ultima fecha modificacion: 24/09/2019
* Descripcion: Controla la cuchilla para cortar y su velodidad

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour{

    // Velocidad de giro
    public float speedTurn;

    // Velocidad de desplazamiento
    public float speedMove;

    // Daño que hará al jugador
    public int damage = 70;

    // Referencia al waypoint actual
    public int actualWaypoint;

    // Referencia al siguiente waypoint
    public int nextWaypoint;

    // Referencia a los waypoints
    public Transform waypoints;

    // Para saber cuantos waypoints hijos detecta
    private int waypointChilds;


    // Start is called before the first frame update
    void Start() {       
        // Guardamos el número de hijos de waypoints
        waypointChilds = waypoints.childCount;
    }

    // Update is called once per frame
    void Update(){
        // Giramos el objeto a la velocidad indicada
        transform.Rotate(Vector3.forward * speedTurn * Time.deltaTime);

        // Llamamos al método que movera la cuchilla
        SpeedMovement();
    }

    private void OnTriggerEnter(Collider other) {
        // Si chocamos con del tag de player, recuperamos su componente playerHealth
        if (other.CompareTag("Player")) {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();

            // Si es distinto de nulo, le hacemos daño
            if(playerHealth != null) {
                playerHealth.TakeDamage(damage);
            }
        }

        
    }

    /// <summary>
    /// Método para mover la cuchilla con velocidad aleatoria
    /// </summary>
    public void SpeedMovement() {

        // Movemos la cuchilla a la velocidad aleatoria hasta el siguiente waypoint
        transform.position = Vector3.MoveTowards(transform.position, waypoints.GetChild(actualWaypoint).position, speedMove * Time.deltaTime);

        // Si estamos en la posición del waypoint, indicamos que el actual es el siguiente waypoint
        if (transform.position == waypoints.GetChild(actualWaypoint).position) {
            actualWaypoint = nextWaypoint;
            nextWaypoint++;
        }

        // Si el siguiente waypoint es mayor que los hijos, indicamos que vuelva al principio, si no, será el asignado
        nextWaypoint = (nextWaypoint >= waypointChilds) ? 0 : nextWaypoint;
    }
}

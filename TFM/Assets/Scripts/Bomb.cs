﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder hacer uso del nav mesh
using UnityEngine.AI;

public class Bomb : MonoBehaviour{
    // Radio de la esfera de detección de daño.
    public float explosionRadius = 3f;

    // Daño aplicado con la exploisión.
    public int damage = 100;

    // Muestra de forma visual el área que cubrirá la explosión.
    public bool drawGizmo = true;

    // Fuerza a la que será lanzado el proyectil.
    public float bulletForce = 20f;

    // Sistema de partículas que hara de efecto de explosión
    public ParticleSystem explosionParticles;

    // Referencia al rigidbody
    private Rigidbody bulletRB;

    // Referencia al capsule collider
    private CapsuleCollider capsuleCollider;


    // Para indicar cuales son los layers contra caules puede colisionar la bala de cañón.
    //public LayerMask collisionLayer;

    // Para controlar que solamente se active la explosión una vez.
    private bool activated;


    void Start() {
        // Recuperamos componente
        bulletRB = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
    }

    void Update() {
        
    }


    public void CallAttack() {
        // Recuperamos el rigidbody de la bomba y le añadimos fuerza hacia adelante
        //bulletRB.AddForce(transform.forward * bulletForce, ForceMode.Impulse);
        bulletRB.useGravity = true;
        bulletRB.isKinematic = false;

        // La desemparentamos 
        transform.SetParent(null);

        // Activamos el collider
        capsuleCollider.enabled = true;
    }

    private void OnTriggerEnter(Collider other) {

        // Si la bala ya ha sido activada, no hacemos nada.
        if (activated) {

            return;
        }

        // Indicamos que la bala ha sido activada, para que no se vuelva a producir el efecto.
        activated = true;

        // Reproducimos el sistema de partículas
        explosionParticles.Play();
        
        // Recuperamos todas las colisiones detectadas dentro del área de la explosión.
        Collider[] colls = Physics.OverlapSphere(transform.position, explosionRadius);

        // Recorremos todas las colisiones detectadas.
        foreach (Collider col in colls) {

            // Intentamos recuperar el componente Enemy Health y nav mesh agent del objeto colisionado.
            //EnemyHealth enemyHealth = col.GetComponent<EnemyHealth>();
            NavMeshAgent navEnemy = col.GetComponent<NavMeshAgent>();

            // Si el objeto colisionado tiene el componente Enemy Health 
            if (navEnemy != null) {
                // Detenemos el movimiento
                navEnemy.isStopped = true;
                //Aplicamos el daño.
                //enemyHealth.TakeDamage(damage);
            }
        }

        // Recuperamos todos los mesh renderer de los hijos de la bala.
        MeshRenderer[] childrenMR = GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer mr in childrenMR) {
            // Desactivamos los mesh renderer para hacerlos invisibles.
            mr.enabled = false;
        }

       
    }

    private void OnDrawGizmos() {

        if (!drawGizmo) {
            return;
        }

        // Dibujamos el área de explosión de la bala.
        Gizmos.DrawSphere(transform.position, explosionRadius);
    }

}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 07/07/2019
* Última fecha modificación: 27/08/2019
* Descripción: Administra las funcionalidades de la espada.

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Sword : MonoBehaviour{

    // Daño que hará la espada
    public int damage;

    // Referencia al animator
    private Animator animator;

    // Referencia al playerController
    private PlayerController playerController;


    // Start is called before the first frame update
    void Start(){
        // Recuperamos componentes
        animator = GetComponentInParent<Animator>();
        playerController = GetComponentInParent<PlayerController>();
    }

    // Update is called once per frame
    void Update(){
        if (!Input.GetButtonUp("Fire1")) {
            playerController.swordAttack = false;
        }
    }

    /// <summary>
    /// Método que será llamado para ejecutar el ataque
    /// </summary>
    public void CallAttack() {
        // Si la energía es mayor de 10, podemos atacar
        if (GameManager.instance.actualEnergy > 10) {
            // Lanzamos la animación de ataque
            animator.SetTrigger("Attack");

            // Activamos la booleana de ataque
            playerController.swordAttack = true;

            // Reducimos la energía indicada
            GameManager.instance.UseEnergy(playerController.swordEnergy);
        }
        
    }

    private void OnTriggerEnter(Collider collider) {

        // Recuperamos el componente EnemyHealth
        EnemyHealth enemyHealth = collider.GetComponent<EnemyHealth>();

        // Si enemyHealth es distinto de null, atacamos y le hacemos daño
        if (enemyHealth != null) {
            enemyHealth.TakeDamage(damage, transform.position);    
        }
    }
}

﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 15/10/2019
* Ultima fecha modificacion: 15/10/2019
* Descripcion: Script que gestiona puntos de control

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour{

    // Variable para indicar si el checkpoint está activado
    public bool isActive = false;

    // Array donde almacenaremos todos los puntos de control de la escena
    public static GameObject[] checkPoints;

    // Referencia al transform storables que contiene los objetos a guardar
    private Transform storables;

    // Animación para el checkpoint
    private Animator persistentAnimator;

    // Partículas para el efecto del checkPoint
    private ParticleSystem checkPointParticles;

    // Start is called before the first frame update
    void Start(){
        // Inicializamos el array
        checkPoints = GameObject.FindGameObjectsWithTag("CheckPoint");

        // Buscamos el sistema de partículas
        checkPointParticles = GameObject.FindGameObjectWithTag("ParticlesCheckPoint").GetComponent<ParticleSystem>();

        // Buscamos el objeto con el animator
        persistentAnimator = GameObject.Find("PersistentCanvas").GetComponent<Animator>();

        // Buscamos el transform de objetos guardables
        storables = GameObject.Find("Storables").transform;
    }

    // Update is called once per frame
    void Update(){
        
    }

    /// <summary>
    /// Método para activar el CheckPoint
    /// </summary>
    private void ActivatedcheckPoint() {
        // Recorremos al array de checkpoints
        foreach(GameObject cP in checkPoints) {
            // Los desactivamos
            cP.GetComponent<Checkpoint>().isActive = false;
        }

        // Activamos el checkPoint actual
        isActive = true;

        // Desactivamos este checkpoint para que no vuelva a pasar por el
        this.gameObject.SetActive(false);

        // Llamamos al método
        GetActiveCheckPoint();
    }


    private void OnTriggerEnter(Collider other) {
        // Si el otro collider es el player, activamos el punto de control
        if (other.CompareTag("Player")) {
            ActivatedcheckPoint();

            // Reproducimos sistema de partículas
            checkPointParticles.Play();

            // Ejecutamos la animación
            persistentAnimator.SetTrigger("CheckPointEffect");
        }
    }

    
    public void GetActiveCheckPoint() {
        
        
        // Si la lista de checkpoints no está vacía
        if(checkPoints != null) {
            // Recorremos todos los checkpoints
            foreach(GameObject cP in checkPoints) {
                // Si el checkpoint está activado
                if(cP.GetComponent<Checkpoint>().isActive) {

                    // Para que no se dupliquen los datos, borramos la lista
                    DataManager.instance.data.storable.Clear();

                    // Guardamos los datos de los objetos guardables
                    SaveStorables();

                    // Guardamos la posición del checkpoint
                    DataManager.instance.data.playerPositionX = cP.transform.position.x;
                    DataManager.instance.data.playerPositionY = cP.transform.position.y;
                    DataManager.instance.data.playerPositionZ = cP.transform.position.z;

                    // Guardamos en el data los valores de la vida y energía
                    DataManager.instance.data.playerEnergy = GameManager.instance.actualEnergy;
                    DataManager.instance.data.playerHealth = GameManager.instance.healthSlider.value;

                    // Guardamos los datos del juego
                    DataManager.instance.Save();
                    break;
                }
            }

            
        }
        
        
    }

    /// <summary>
    /// Método para guardar los objetos en el data
    /// </summary>
    private void SaveStorables() {
        // Recorremos todos los objetos en la lista de storables
        foreach(Transform storable in storables) {
            
            // Añadimos un nuevo objeto con su nombre
            DataManager.instance.data.storable.Add(new Storable() {name = storable.name,                            // Guardamos e
                                                                   objectPositionX = storable.transform.position.x, // Guardamos su posición en X
                                                                   objectPositionY = storable.transform.position.y, // Guardamos su posición en Y
                                                                   objectPositionZ = storable.transform.position.z, // Guardamos su posición en Z
                                                                   state = storable.gameObject.activeSelf});        // Guardamos su estado
        }
    }
}

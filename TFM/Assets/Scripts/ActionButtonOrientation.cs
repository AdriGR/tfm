﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 24/09/2019    
* Ultima fecha modificacion: 25/09/2019
* Descripcion: Para que el botón que tiene que presionar esté siempre mirando a la cámara

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionButtonOrientation : MonoBehaviour{

    // Referencia a la cámara
    private Camera cam;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componente
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update(){
        // Hacemos que la imagen mire hacia la cámara
        //transform.LookAt(cam.transform.position);
        transform.rotation = Quaternion.LookRotation(cam.transform.forward);

    }

    
}

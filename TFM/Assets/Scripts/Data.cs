﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Data{

    // Nombre de la escena actual
    public string actualScene;
    // Nombre del punto de entrada a la escena
    public string entrancePosition;
    // Array con el estado de todas las condiciones del juego
    public Condition[] allCondition;
    // Array con el estado de todos los items del juego
    public Item[] allItem;
    // Array con el inventario actual del jugador, con el número limitado de slots
    public Item[] inventory = new Item[4];

    // Lista de objetos guardables
    public List<Storable> storable = new List<Storable>();

    // Posición del jugador
    public float playerPositionX;
    public float playerPositionY;
    public float playerPositionZ;

    // Vida del jugador
    public float playerHealth;

    // Energía del jugador
    public float playerEnergy;

    // Volumen del sonido
    public float soundVolume;

    // Volumen de los efectos de sonido
    public float effectsVolume;

    // Velocidad de dialogos
    public float speedText;



}

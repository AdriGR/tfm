﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 03/09/2019
* Ultima fecha modificacion: 03/09/2019
* Descripcion: Interactable para administrar los items

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableItem : Interactable {

    // Nombre del item para localizarlo en el listado de AllItems en el Data del dataManager
    public string nameItem;

    // Start is called before the first frame update
    protected override void Start() {
        // Ejecutamos el método del que hemos heredado
        base.Start();

        

        // Verificamos si el objeto ya ha sido recogido y si ha sido así, lo desactivamos
        if(IsPicked()) {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update(){
        
    }

    bool IsPicked() {
        // Recorremos el listado de items
        foreach(Item item in DataManager.instance.data.allItem) {
            // Si el nombre del item coincide, devolvemos el estado en el que se encuentra
            if (item.name == nameItem) {
                return item.picked;
            }
        }

        // En el caso de no encontrar el nombre del item, mostramos un mensaje de advertencia de que no existe y devolvemos falso 
        Debug.LogWarning("El nombre del item no existe en la lista de items: " + nameItem);
        return false;
    }


    

    
}

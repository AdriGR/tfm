﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour{

    // Referencia al array de la jerarquia de los waypoints
    public Transform waypoints;
    // Referencia a los waypoints
    public int nextWaypoint;

    // Para saber si la plataforma se está moviendo
    public bool movingPlatform;
    // Referencia al waypoint actual
    public int actualWaypoint;
    // Referencia a la velocidad a la cual se moverá la plataforma
    public float speed;

    public int waypointChild;


    // Start is called before the first frame update
    void Start(){
        // Indicamos que la plataforma no se mueve

        // Guardamos la longitud del array de hijos creados en la jerarquia
        //waypoints = transform.Find("Waypoints");
        // igualamos el actualWaypoint al primer hijo, que será desde el cual empezaremos
        actualWaypoint = 0;

        Debug.Log(waypoints);

        waypointChild = waypoints.childCount;

        

    }

    // Update is called once per frame
    void Update(){
        if (movingPlatform) {
            NextWaypoint();
        }

        
    }



    /// <summary>
    /// Método para comprobar
    /// </summary>
    public void NextWaypoint() {

        // indicamos que el hijo que coja sea el actualwaypoint, en este caso el 0 (primer hijo)
        transform.position = Vector3.MoveTowards(transform.position, waypoints.GetChild(actualWaypoint).position, speed * Time.deltaTime);
       

       if (transform.position == waypoints.GetChild(actualWaypoint).position) {
            actualWaypoint = nextWaypoint;
            nextWaypoint++;
            
            
       }

        nextWaypoint = (nextWaypoint >= waypointChild) ? 0 : nextWaypoint;
      
    }


    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            other.transform.SetParent(transform);
            
            
            
        }
    }


    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")) {
            other.transform.parent = null;
        }
    }

}

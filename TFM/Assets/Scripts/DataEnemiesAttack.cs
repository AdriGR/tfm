﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class DataEnemiesAttack{

    // Daño que realiza
    public int damage;

    // Distancia a la que puede atacar
    public float attackDistance;

    // Tiempo que debe de pasar para que pueda volver a atacar
    public float timeToAttack;

    // Clip de sonido del ataque
    public AudioClip attackSound;

    // Tiempo que se quedará paralizado el enemigo
    public float paralizedTime;
    



}

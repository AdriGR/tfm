﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creación: 16/08/2019
* Última fecha modificación: 08/10/2019
* Descripción: Permite interactuar con objetos pulsando el botón de acción

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputButton : MonoBehaviour{

    // Botón de interacción
    public GameObject interactButton;

    // Booleana para indicar si puede interactuar con el objeto
    public bool canInteract = false;

    // Referencia al interactable
    private Interactable interactable;


    // Start is called before the first frame update
    void Start(){
        // Recuperamos el componente
        interactable = GetComponent<Interactable>();
       
    }

    // Update is called once per frame
    void Update(){
        // Si podemos interactuar y pulsamos el botón de interacción, llamamos al método que realiza la acción
        if(canInteract && Input.GetButtonDown("Interaction")) {
            interactable.Interact();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            // Indicamos que puede interactuar
            canInteract = true;

            // Si la otra etiqueta es igual a Player, activamos la imagen del botón a pulsar para poder realizar la interacción
            interactButton.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")) {
            // Indicamos que no puede interactuar
            canInteract = false;

            // Si sale del collider otro objeto con la etiqueta Player, desactivamos la imagen del botón a pulsar
            interactButton.SetActive(false);
        }
    }


}

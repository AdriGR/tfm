﻿/************************************************************************************************************
* Fichero creado por: Adri
* Fecha creacion: 18/09/2019
* Ultima fecha modificacion: 18/09/2019
* Descripcion: Modifica la escala del charco postmortem y su tiempo en juego hasta desaparecer

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blood : MonoBehaviour{

    // Escala máxima de la sangre
    public float bloodScale = 0.08f;

    // Cantidad en la que aumentará la sangre
    public float showBlood = 0.001f;

    // Temporizador para indicar cuando se disolvera la sangre
    public float timerBlood;

    // Array de imagenes para cuando muera
    public Sprite[] deathBloodImages;
    
    // Temporizador interno para disolver el charco
    private float temp = 0;

    // Referencia a la imagen de sangre
    private SpriteRenderer bloodImage;

    // Start is called before the first frame update
    void Start(){
        // Recuperamos componente
        bloodImage = GetComponent<SpriteRenderer>();

        // Asignamos un número aleatorio para el array entre su principio (0) y su máximo
        int randomBlood = Random.Range(0, deathBloodImages.Length);

        // Cargamos la imagen aleatoria del array en el sprite
        bloodImage.sprite = deathBloodImages[randomBlood];
    }

    // Update is called once per frame
    void Update(){
        // Si la escala en x, y, z es menor que el tamaño de escala definido
        if (bloodImage.transform.localScale.x < bloodScale && bloodImage.transform.localScale.y < bloodScale && bloodImage.transform.localScale.z < bloodScale) {
            // Aumentamos la escala poco a poco en la cantidad indicada
            bloodImage.transform.localScale += new Vector3(showBlood, showBlood, showBlood);
            // Si la escala en x, y, z es mayor o igual al tamaño indicado
        }
        else if (bloodImage.transform.localScale.x >= bloodScale && bloodImage.transform.localScale.y >= bloodScale && bloodImage.transform.localScale.z >= bloodScale) {
            // Incrementamos el contador
            temp += Time.deltaTime;
        }

        // Si el contador es mayor o igual al tiempo indicado
        if (temp >= timerBlood) {
            // Hacemos que la mancha vaya desapareciendo poco a poco
            bloodImage.color = Color.Lerp(bloodImage.color, Color.clear, Time.deltaTime);
        }
    }
}

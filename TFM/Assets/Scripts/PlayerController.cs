﻿/************************************************************************************************************
* Fichero creado por: Adri - Youssef
* Fecha creación: 23/04/2019
* Última fecha modificación: 26/09/2019
* Descripción: Controla los movimientos y acciones del jugador

*************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para usar elementos del HUD
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    [Header("Caracteristicas Player")]
    // Variable para la velocidad del jugador
    public float speed;

    public float aceleration = 1000f;
    // Velocidad para correr.
    public float runSpeed = 8f;
    // Velocidad para andar
    public float walkSpeed = 4f;
    // Velocidad de idle
    public float idleSpeed = 0f;
    // Fuerza del salto
    public float jumpForce;
    // Variable para controlar si el jugador puede saltar
    public bool jumped = false;

    // para saber si se está moviendo
    public bool isWalking = false;
    // Para saber si estamos corriendo
    public bool isRun = false;

    //Bool para saber si está atacando con la espada.
    public bool swordAttack;
    // Objeto vacío que indicará cual será el forward y el backward del player
    private GameObject directionMove;
    // Objeto con el que se va a interactuar
    private Interactable currentInteractable;
    // Para saber cuando el jugador está bloqueado y puede interactuar y/o moverse
    public bool blockMove = false;

    // Referencia a las armas u objetos si están activados o no
    [Header("Armas/Objetos")]
    public GameObject sword;
    public bool swordActive;
    public GameObject shield;
    public bool shieldActive;
    public GameObject bomb;
    public bool bombActive;
    public GameObject gun;
    public bool gunActive;

    public int bullets = 0;
    public float recoilForce = 2.5f;
    public Transform shotPosition;

    // Para llevar la temporización del cooldown.
    public float timer;

    // Prefab de la bala que será disparada.
    public GameObject bulletPrefab;



    public float coolDown = 2f;

    [Header ("Usos energía")]

    // Cantidad de energía que consume correr
    public int runEnergy;

    // Cantidad de energía que consume usar el escudo
    public int shieldEnergy;

    // Cantidad de energía que consume atacar con la espada
    public int swordEnergy;

    [Header("Booleanas")]
    // Para saber si estamos tocando el suelo
    public bool isGrounded;
    // Para saber si estamos tocando una plataforma
    public bool isPlatform;

    [Header("Detección suelo")]
    // Para la detección del suelo
    public float widthGroundCheck;
    public float heightGroundCheck;
    public float deepGroundCheck;
    // layer para el suelo y plataforma
    public LayerMask groundMask;
    public LayerMask platformMask;
    // posición donde detectará el suelo
    public Transform groundMark;

    [Header("Sonidos")]
    // Sonido de cuando golpeamos pero no le damos a nada
    public AudioClip missingPunch;

    // Sonido de salto
    public AudioClip jumpSound;


    // Referencia al rigidbody
    private Rigidbody rB;

    // Referencia al vector de movimiento
    private Vector3 movement;

    // Referencia al animator
    private Animator animator;

    // Referencia al audioSource
    private AudioSource audioSource;

    // Referencia al playerHealth
    private PlayerHealth playerHealth;

    // Referencia a la cámara para desactivar el target en el caso que caiga al vacio
    private Camera_track cameraTrack;

    // Use this for initialization
    void Start() {
        // Recuperamos componentes
        rB = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();
        playerHealth = GetComponent<PlayerHealth>();
        cameraTrack = GameObject.Find("MainCamera").GetComponent<Camera_track>();

        // Buscamos el objeto vacío el cual hace referencia a la dirección forward/backward del player
        directionMove = GameObject.Find("MovementPlayer");

        // Empezamos parados
        isWalking = false;

        // Creamos un nuevo vector para almacenar los datos de posición del dataMánager
        Vector3 positionSaved = new Vector3(DataManager.instance.data.playerPositionX, DataManager.instance.data.playerPositionY, DataManager.instance.data.playerPositionZ);

        // Si la posición almacenada es distinta de 0,0,0, la posición del jugador será la guardada, si no, será la que tenga por defecto al iniciar la escena
        transform.position = (positionSaved != Vector3.zero) ? positionSaved : transform.position;

        
       
    }

    // Update is called once per frame
    void Update() {

        // Pasamos al animator el valor de speed para el blend tree
        animator.SetFloat("Speed", rB.velocity.magnitude);

        Debug.Log(rB.velocity.magnitude);

        // si pulamos el boton de salto y estamos tocando el suelo o plataforma, saltamos
        if (Input.GetButtonDown("Jump") && (isGrounded || isPlatform )) {
            /// Añadimos fuerza hacia arriba
            rB.velocity = Vector3.up * jumpForce;

            // Ejecutamos la animación de saltar
            animator.SetTrigger("Jump");

            // Reproducimos el sonido de salto
            audioSource.PlayOneShot(jumpSound);
        }

        // Si está el escudo activo, llamamos a su animación
        if(shieldActive) {
            animator.SetBool("Shield", true);
        } else {
            // Si no, indicamos que no la realice
            animator.SetBool("Shield", false);
        }

        // Si pulsamos el botón de ataque y no tenemos activo ningun arma ni estamos en pausa, lanzamos la animación de puñetazo
        if (Input.GetButtonDown("Fire1") && !swordActive && !gunActive && !bombActive && !GameManager.instance.pause) {
            animator.SetTrigger("Punch");

            // Reproducimos el sonido de golpear
            audioSource.PlayOneShot(missingPunch);

            // si no es asi y pulsamos el boton de ataque, hacemos una llamada a los hijos al método de atacar
        } else if (Input.GetButtonDown("Fire1") && !GameManager.instance.pause){
            BroadcastMessage("CallAttack");
        }

        // Obtenemos los valores de los ejes horizontal y vertical del control
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // Si no estamos bloqueados, llamamos al método que mueve el personaje
        if(!blockMove) {
            Move(h, v);
        }
        

        // Llamamos al método de cambiar de armas
        ChangeWeapons();



        // Si pulsamos el botón de correr y estamos moviendonos
        if (Input.GetButton("Run") && isWalking) {
            // Si la energía es mayor de 10
            if (GameManager.instance.actualEnergy > 0) {

                // Indicamos que estamos corriendo
                isRun = true;

                // Indicamos que es un gasto de energía continuada
                GameManager.instance.continuedEnergy = true;

                // Reducimos energía
                GameManager.instance.UseEnergy(runEnergy);
            } 


        }

        // Si levantamos el botón de correr o no nos estamos moviendo o la energía es 0
        if (Input.GetButtonUp("Run") || !isWalking || GameManager.instance.actualEnergy <= 0) {

            // Cambiamos al velocidad a la de andar
            speed = walkSpeed;
            
            // Indicamos que ya no estamos corriendo
            isRun = false;

            
            
        }

        if(isRun) {
            // Si estamos corriendo, indicamos que tenemos velocidad de correr
            speed = runSpeed;
        } else if(isWalking) {
            // Si no, si estamos andando, indicamos que tenemos velocidad de andar
            speed = walkSpeed;
        } else {
            // Si no, tenemos velocidad de idle (0)
            speed = idleSpeed;
            
        }
        
        // Si el escudo está activo, y el nivel de energía es mayor que 10
        if(shieldActive) {
            if (GameManager.instance.actualEnergy > 0) {
                // Reducimos la energía indicada
                GameManager.instance.UseEnergy(shieldEnergy);
            } else {
                // Si no desactivamos el escudo y su booleana de estar activo
                shieldActive = false;
                shield.SetActive(false);
                // Indicamos que el uso continuado de energía en el escudo ya no se utiliza
                GameManager.instance.continuedEnergy = false;
            }
             
        }

        // Si no estamos haciendo uso de nada que consuma energía, la vamos recuperando
        if (!isRun && !shieldActive && !swordAttack) {
            GameManager.instance.RecoveryEnergy();
        }

        // Si no estamos andando, indicamos que la velocidad es 0.
        if(!isWalking) {
            speed = idleSpeed;
            
        } 

    }


    public void FixedUpdate() {
        // Creamos un overlapBox para detectar si estamos tocando el suelo
        Collider[] colls = Physics.OverlapBox(groundMark.position, new Vector3(widthGroundCheck, heightGroundCheck, deepGroundCheck), Quaternion.identity, groundMask);
        // Si la longitud del array es mayor que 0, indicamos que está sobre el suelo
        isGrounded = colls.Length > 0;

        // Creamos un overlapBox para detectar si estamos tocando una plataforma
        Collider[] collPlatform = Physics.OverlapBox(groundMark.position, new Vector3(widthGroundCheck, heightGroundCheck, deepGroundCheck), Quaternion.identity, platformMask);

        // Si la longitud del array es mayor que 0, indicamos que está sobre una plataforma
        if (collPlatform.Length > 0) {
            isPlatform = true;
        }
    }

    /// <summary>
    /// Método para usar el botón de acción (en este caso la tecla E)
    /// </summary>
    /// <param name="interactable"></param>
    public void OnInteractableButton(Interactable interactable) {
        // Si el jugador está bloqueado, salimos
        if(blockMove) {
            return;
        }

        // indicamos que el objeto recibido por parámetro, es con el que interactuamos
        currentInteractable = interactable;

        // rotamos hacia la posición de la interacción
        transform.rotation = interactable.interactionLocation.rotation;

        
        
    }

    
    /// <summary>
    /// Método para mover el personaje
    /// </summary>
    void Move(float h, float v) {

        // Trasladamos el movimiento al plano, haciendo que ignore la y
        movement.Set(h, 0f, v);

        // Orientamos la dirección del movimiento a la rotación del objeto
        movement = directionMove.transform.rotation * movement;

        // Normalizamos el vector
        movement = movement.normalized;

        // Calculamos la fuerza segun la aceleración definida
        movement = movement * speed * Time.deltaTime;

        if (rB.velocity.magnitude < speed) {
            // rB.MovePosition(transform.position + movement);
            rB.velocity = new Vector3(movement.x, rB.velocity.y, movement.z);
        }

        // si el jugador está solicitando un desplazamiento en alguna dirección
        if (Mathf.Abs(h) > 0 || Mathf.Abs(v) > 0) {
            Vector3 temp = new Vector3(h, 0f, v);
            // Indicamos que nos estamos moviendo
            isWalking = true;

            // realizamos la rotación del personaje de forma gradual, en la dirección del desplazamiento solicitado
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement), 0.2f);
        } else {
            isWalking = false;
        }
    }
    

    
    private void OnTriggerEnter(Collider other) {
        

        // Si colisionamos con un objeto con el tag deadzone
        if (other.CompareTag("DeadZone")){
            // Llamamos al método de muerte
            playerHealth.Death();

            // Quitamos el target a la cámara para que no siga a nada
            cameraTrack.target = null;

            
        }

        


    }
    

    /// <summary>
    /// Método para cambiar de armas u objetos en el inventario
    /// </summary>
    public void ChangeWeapons() {

        // Si pulsamos el botón 1 y hemos cogido el arma
        if (Input.GetKeyDown(KeyCode.Alpha1) && (DataManager.instance.data.inventory[0].picked)) {
            // Activamos o desactivamos la espada según su estado
            sword.SetActive(!sword.activeSelf);

            // Activamos/desactivamos la booelana de control segun su estado
            swordActive = !swordActive;
        }

        // Si pulsamos el botón 2 y hemos cogido el arma
        if (Input.GetKeyDown(KeyCode.Alpha2) && (DataManager.instance.data.inventory[1].picked)) {
            // Activamos o desactivamos la pistola según su estado actual
            gun.SetActive(!gun.activeSelf);

            // Activamos o desactivamos la booleana de control segun su estado
            gunActive = !gunActive;
        }

        // Si pulsamos el botón 3 y hemos cogido el arma
        if (Input.GetKeyDown(KeyCode.Alpha3) && (DataManager.instance.data.inventory[2].picked)) {
            // Activamos/desactivamos el escudo según su estado
            shield.SetActive(!shield.activeSelf);
            // Activamos/desactivamos la booleana de control segun su estado
            shieldActive = !shieldActive;

            // Indicamos que el uso de energía continuada será igual a si está o no activado el escudo
            GameManager.instance.continuedEnergy = shieldActive;
        }

        // Si pulsamos el botón 4 y hemos cogido el arma
        if (Input.GetKeyDown(KeyCode.Alpha4) && (DataManager.instance.data.inventory[3].picked)) {
            bomb.SetActive(!bomb.activeSelf);
            bombActive = !bombActive;
        }
    }


    /// <summary>
    /// Realizará las acciones necesarias para disparar el cañón.
    /// </summary>
    void Shoot() {
        if (bullets > 0) {
            // Reiniciamos el cooldown en el momento de disparar.
            timer = coolDown;



            // Instanciamos la bala de cañón alineada con la orientación del cañón.
            GameObject tempBullet = Instantiate(bulletPrefab, shotPosition.position, Quaternion.LookRotation(transform.forward));



            // Aplicamos el retroceso.
            rB.AddForce(-transform.forward * recoilForce, ForceMode.Impulse);

            bullets -= 1;
        }
    }

    private void OnDrawGizmos() {

        Gizmos.DrawCube(groundMark.position, new Vector3(widthGroundCheck, heightGroundCheck, deepGroundCheck));
    }
}

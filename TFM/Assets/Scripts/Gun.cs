﻿/***************************************************************************************************
 * Fichero creado por: Adri
 * Fecha creación: 09 Julio 2019
 * Ultima fecha modificación: 11 Julio 2019
 * Descripción: El script permite al activarse la pistola mover el brazo en 180º en el forward del jugador, disparar y recargar actualizando el hud de munición
 ***************************************************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para usar elementos del hud
using UnityEngine.UI;

public class Gun : MonoBehaviour {

    // Transform desde el cual lanzará el disparo
    public Transform shootPosition;

    // Objeto que moveremos para manejar el disparo
    public GameObject arm;

    // Rango que alcanza el arma
    public int range;

    // Daño que hará el disparo
    public int damage;



    [Header("Munición")]
    // Texto referente a la munición actual
    public Text actualAmmoText;

    // Cantidad actual de munición
    public int actualAmmo;

    // Texto referente a la munición máxima
    public Text AmmoText;

    // Cantidad de munición del cargador
    public int ActualAmmoCharger;

    // Máxima munición posible(fijo)
    public int maxAmmo;

    // Cantidad máxima de munición que permite el cargador (fijo)
    public int maxAmmoByCharger;

    // Panel que indica la munición
    public GameManager ammoPanel;

    [Header("Sonidos")]
    // Sonido para cuando no quedan balas
    public AudioClip noAmmoSound;

    // Sonido para cuando dispara
    public AudioClip shootSound;

    // Distancia que alcanzará el raycast que lanzamos desde la cámara para detectar objetos con los que impacta
    public float distanceRayCursor;

    // Layers para detectar el suelo
    public LayerMask groundMask;

    // Layers para los elementos impactables
    public LayerMask shootableMask;


    // Para detectar los impactos
    private Ray shootRay;

    // Referencia al line Renderer que simulará la bala
    private LineRenderer shootRenderer;

    // Referencia al audio
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start() {
        // Recuperamos los componentes
        shootRenderer = GetComponentInChildren<LineRenderer>();
        audioSource = GetComponentInChildren<AudioSource>();

        // Inicializamos los valores
        ActualAmmoCharger = maxAmmoByCharger;
    }

    // Update is called once per frame
    void Update() {
        // LLamamos al método para girar el brazo
        TurningArm();
    }

    /// <summary>
    /// Método para girar el brazo
    /// </summary>
    public void TurningArm() {
        // Realizamos un rayo desde el cursor hacia el mundo para que pueda seguirlo
        Ray cursorRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Almacenamos la variable del raycast
        RaycastHit raycastHit = new RaycastHit();

        // detectamos el punto donde impacta en el mundo
        if (Physics.Raycast(cursorRay, out raycastHit, distanceRayCursor, groundMask)) {

            // Calculamos la dirección a la que tiene que mirar la torreta, restando la posición de origen al punto de impacto
            Vector3 armToMouse = raycastHit.point - arm.transform.position;

            // Nos aseguramos de eliminar el eje Y para no tener inclinaciones
            armToMouse.y = 0f;

            // Calculamos la rotación del brazo para que mire hacia el cursor (ya calculado antes)
            Quaternion newRotation = Quaternion.LookRotation(armToMouse);

            // Giramos el brazo
            arm.transform.rotation = newRotation;
        }
    }


    /// <summary>
    /// Método que será llamado para ejecutar el ataque, en este caso el disparo de la pistola
    /// </summary>
    public void CallAttack() {
        // Si no quedan balas, reproducimos sonido de no munición
        if (actualAmmo <= 0) {
            audioSource.PlayOneShot(noAmmoSound);
        }

        // Reducimos munición
        actualAmmo--;

        // Reproducimos el sonido de disparo
        audioSource.PlayOneShot(shootSound);

        // Activamos el dibujado del disparo
        shootRenderer.enabled = true;

        // Indicamos de donde va a salir el disparo
        shootRenderer.SetPosition(0, shootPosition.position);

        // Indicamos el origen del rayo
        shootRay.origin = shootPosition.position;

        // Indicamos la dirección del rayo
        shootRay.direction = shootPosition.forward;

        // Almacenamos la información del impacto en una variable
        RaycastHit shootHit;

        // Realizamos el raycast
        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask)) {
            // Si llega a impactar, terminamos la linea en dicho impacto
            shootRenderer.SetPosition(1, shootHit.point);

            // Intentamos recuperar el componente enemyHealth
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();

            // Si contiene el componente hemos impactado contra un enemigo y le indicamos donde y el daño producido
            if (enemyHealth != null) {
                enemyHealth.TakeDamage(damage, shootHit.point);
            } else {
                // Si no, dibujamos la linea con el rango indicado
                shootRenderer.SetPosition(1, shootRay.direction * range);
            }
        }
    }

    /// <summary>
    /// Método que será llamado para ejecutar la recarga de la pistola.
    /// </summary>
    public void CallReload() {

        // Si el cargador es mayor o igual a la cantidad que pueda almacenar, salimos
        if (actualAmmo >= maxAmmoByCharger) {
            return;
        }

        // Si la cantidad atual de munición del cargador está por debajo de la capacidad máxima
        if (ActualAmmoCharger < maxAmmoByCharger) {

            // Actualizamos el valor de la munición máxima
            actualAmmo -= ActualAmmoCharger;

            // Recargamos solo la munición permitida
            actualAmmo += maxAmmoByCharger - actualAmmo;



            // Si la cantidad máxima de munición queda por debajo de 0, obligamos a que sea 0
            if (maxAmmo <= 0) {
                maxAmmo = 0;
            }
        } else {

            // Reducimos la cantidad total de balas en el número de balas que haya recargado
            maxAmmo -= maxAmmoByCharger - actualAmmo;

            // Igualamos la munición del cargador a la máxima permitida por cargador
            actualAmmo = maxAmmoByCharger;
        }


        // Actualizamos el hud
        AmmoText.text = ActualAmmoCharger.ToString();
        actualAmmoText.text = actualAmmo.ToString();

    }
}

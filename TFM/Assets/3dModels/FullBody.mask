%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: FullBody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: BYRON
    m_Weight: 1
  - m_Path: byron_esqueleto
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_L
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_L/brazo_2_L
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_L/brazo_2_L/mano_L
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_L/brazo_2_L/mano_L/mano_L_end
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_R
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_R/brazo_2_R
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_R/brazo_2_R/mano_R
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/brazo_1_R/brazo_2_R/mano_R/mano_R_end
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/cabeza
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/columna_1/columna_2/cabeza/cabeza_end
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/pierna_1_L
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/pierna_1_L/pierna_2_L
    m_Weight: 0
  - m_Path: byron_esqueleto/cadera/pierna_1_L/pierna_2_L/pie_L
    m_Weight: 0
  - m_Path: byron_esqueleto/cadera/pierna_1_L/pierna_2_L/pie_L/pie_L_end
    m_Weight: 0
  - m_Path: byron_esqueleto/cadera/pierna_1_R
    m_Weight: 1
  - m_Path: byron_esqueleto/cadera/pierna_1_R/pierna_2_R
    m_Weight: 0
  - m_Path: byron_esqueleto/cadera/pierna_1_R/pierna_2_R/pie_R
    m_Weight: 0
  - m_Path: byron_esqueleto/cadera/pierna_1_R/pierna_2_R/pie_R/pie_R_end
    m_Weight: 0
